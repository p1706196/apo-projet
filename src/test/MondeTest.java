/**
 *
 */
package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import eugenisme.model.world.Case;
import eugenisme.model.world.Fourmiliere;
import eugenisme.model.world.Monde;
import eugenisme.model.world.SourceDeNourriture;

/**
 *
 */
class MondeTest {

    /**
     * @throws java.lang.Exception
     */

    private Monde mondeTest;

    @BeforeAll
    static void setUpBeforeClass() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterAll
    static void tearDownAfterClass() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @BeforeEach
    void setUp() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterEach
    void tearDown() throws Exception {
        mondeTest = null;
    }


    @Test
    void testDistance() {
        mondeTest = new Monde(5, 5);
        assertTrue(mondeTest.getDistance(0, 0, 0, 4) == 1
                && mondeTest.getDistance(0, 0, 4, 0) == 1
                && mondeTest.getDistance(0, 0, 4, 4) == 2
                && mondeTest.getDistance(0, 0, 2, 2) == 4);
    }

    @Test
    void testLoadFromFile() {
        mondeTest = new Monde(2, 2);
        try {
            if (mondeTest.loadFromFile("ressources/mondeTest3.txt")
                    && mondeTest.getHeight() == 2
                    && mondeTest.getWidth() == 2
                    && mondeTest.getCase(0, 0).toString().contentEquals("n2")
                    && mondeTest.getCase(0, 1).toString().contentEquals("c")
                    && mondeTest.getCase(1, 0).toString().contentEquals("f2")
                    && mondeTest.getCase(1, 1).toString().contentEquals("f0")) {
                assertTrue(true);
            } else {
                fail("Le monde n'a pas �t� charg� correctement.");
            }
        } catch (Exception e) {
            fail("Le monde n'a pas �t� charg� correctement.");
        }
    }

    @Test
    void testEnleverSourcesVides() {
        mondeTest = new Monde(2, 2);
        try {
            if (mondeTest.loadFromFile("ressources/mondeTest4.txt")
                    && mondeTest.getCase(1, 0).toString().contentEquals("n0")
                    && mondeTest.getCase(1, 1).toString().contentEquals("n0")) {
                mondeTest.enleverSourcesVides();
                if (mondeTest.getCase(1, 0).toString().contentEquals("c")
                        && mondeTest.getCase(1, 1).toString().contentEquals("c")) {
                    assertTrue(true);
                }
            } else {
                fail("Le monde n'a pas �t� charg� correctement.");
            }
        } catch (Exception e) {
            fail("Le monde n'a pas �t� charg� correctement.");
        }
    }

    @Test
    void testToString() {
        mondeTest = new Monde(2, 4);
        mondeTest.loadFromFile("ressources/mondeTest2.txt");
        assertTrue(mondeTest.toString().equals("n1 n2 f0 f0 \nf2 f2 n2 n1 \n"));
    }

    @Test
    void testSaveToFile() {
        mondeTest = new Monde(5, 5);
        assertTrue(mondeTest.saveToFile("ressources/mondeTest.txt"));
    }

    @Test
    void testDistanceMax() {
        mondeTest = new Monde(5, 5);
        assertTrue(mondeTest.getDistanceMax() == 5);
    }

    @Test
    void testCompteursFourmiliere() {
        mondeTest = new Monde(2, 4);
        mondeTest.loadFromFile("ressources/mondeTest2.txt");
        assertTrue(mondeTest.getCompteursFourmilieres() == 4);
    }

    @Test
    void testClone() {
        mondeTest = new Monde(2, 2);
        mondeTest.setCase(0, 0, new SourceDeNourriture(2));
        mondeTest.setCase(0, 1, new Case());
        mondeTest.setCase(1, 0, new Fourmiliere(2));
        mondeTest.setCase(1, 1, new Fourmiliere());
        Monde mondeTest2;
        mondeTest2 = mondeTest.clone();
        if (mondeTest2.getHeight() == 2
                && mondeTest2.getWidth() == 2
                && mondeTest2.getCase(0, 0).toString().contentEquals("n2")
                && mondeTest2.getCase(0, 1).toString().contentEquals("c")
                && mondeTest2.getCase(1, 0).toString().contentEquals("f2")
                && mondeTest2.getCase(1, 1).toString().contentEquals("f0")) {
            assertTrue(true);
        } else {
            fail("Le monde n'a pas �t� copi� correctement.");
        }

    }
}
	
