/**
 *
 */
package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import eugenisme.model.world.Case;
import eugenisme.model.world.Fourmiliere;
import eugenisme.model.world.SourceDeNourriture;

/**
 *
 */
class CaseTest {

    /**
     * @throws java.lang.Exception
     */
    private Case c;
    private Fourmiliere f;
    private SourceDeNourriture n;

    @BeforeAll
    static void setUpBeforeClass() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterAll
    static void tearDownAfterClass() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @BeforeEach
    void setUp() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterEach
    void tearDown() throws Exception {
        c = null;
        f = null;
        n = null;
    }

    @Test
    void testCaseEstFourmiliere() {
        c = new Case();
        assertFalse(c.isFourmiliere());
    }

    @Test
    void testSourceDeNourritureEstFourmiliere() {
        n = new SourceDeNourriture(1);
        assertFalse(n.isFourmiliere());
    }

    @Test
    void testFourmiliereEstFourmiliere() {
        f = new Fourmiliere();
        assertTrue(f.isFourmiliere());
    }

    @Test
    void testCaseEstSourceDeNourriture() {
        c = new Case();
        assertFalse(c.isSourceDeNourriture());
    }

    @Test
    void testSourceDeNourritureEstSourceDeNourriture() {
        n = new SourceDeNourriture(1);
        assertTrue(n.isSourceDeNourriture());
    }

    @Test
    void testFourmiliereEstSourceDeNourriture() {
        f = new Fourmiliere();
        assertFalse(f.isSourceDeNourriture());
    }

    @Test
    void testCaseToString() {
        c = new Case();
        assertTrue(c.toString().contentEquals("c"));
    }

    @Test
    void testSourceDeNourritureToString() {
        n = new SourceDeNourriture(5);
        assertTrue(n.toString().contentEquals("n5"));
    }

    @Test
    void testFourmiliereToString() {
        f = new Fourmiliere(5);
        assertTrue(f.toString().contentEquals("f5"));
    }

    @Test
    void testFourmiliereDeposerNourriture() {
        f = new Fourmiliere();
        f.deposerNourriture();
        assertTrue(f.getCompteurNourriture() == 1);
    }

    @Test
    void testSourceDeNourritureRetirer() {
        n = new SourceDeNourriture(5);
        n.retirerNourriture();
        assertTrue(n.getNourritureRestante() == 4);
    }

    void testSourceDeNourritureRetirerVide() {
        n = new SourceDeNourriture(1);
        assertTrue(n.retirerNourriture());
    }

    @Test
    void testSourceDeNourritureAjouter() {
        n = new SourceDeNourriture(5);
        n.ajouterNourriture();
        assertTrue(n.getNourritureRestante() == 6);
    }
}
