/**
 *
 */
package test;

import static org.junit.jupiter.api.Assertions.*;


import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import eugenisme.model.ant.Fourmi;
import eugenisme.model.ant.Fourmi.Direction;
import eugenisme.model.world.Fourmiliere;
import eugenisme.model.world.Monde;
import eugenisme.model.world.SourceDeNourriture;

/**
 *
 */
class FourmiTest {

    /**
     * @throws java.lang.Exception
     */
    private Fourmi fourmi;
    private Monde monde;

    @BeforeAll
    static void setUpBeforeClass() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterAll
    static void tearDownAfterClass() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @BeforeEach
    void setUp() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterEach
    void tearDown() throws Exception {
        monde = null;
        fourmi = null;
    }

    @Test
    void testDetecterNourritureFail() {
        monde = new Monde(3, 3);
        fourmi = new Fourmi(null);
        fourmi.initialize(monde);
        assertFalse(fourmi.detecterNourriture());
    }

    @Test
    void testDetecterNourritureSuccess() {
        monde = new Monde(3, 3);
        monde.setCase(1, 1, new SourceDeNourriture(1));
        fourmi = new Fourmi(null);
        fourmi.initialize(monde);
        assertTrue(fourmi.detecterNourriture());
    }

    @Test
    void testDetecterFourmiliereFail() {
        monde = new Monde(3, 3);
        fourmi = new Fourmi(null);
        fourmi.initialize(monde);
        assertFalse(fourmi.detecterFourmiliere());
    }

    @Test
    void testDetecterFourmiliereSuccess() {
        monde = new Monde(3, 3);
        monde.setCase(1, 1, new Fourmiliere());
        fourmi = new Fourmi(null);
        fourmi.initialize(monde);
        assertTrue(fourmi.detecterFourmiliere());
    }

    @Test
    void testDeplacementUP() {
        monde = new Monde(3, 3);
        fourmi = new Fourmi(null);
        fourmi.initialize(monde);
        //La fourmi spawn aux coordonn�es 1/1
        fourmi.deplacement(Direction.UP);
        assertEquals(fourmi.getPosH(), 0);
        fourmi.deplacement(Direction.UP);
        assertEquals(fourmi.getPosH(), 2);
    }

    @Test
    void testDeplacementDOWN() {
        monde = new Monde(3, 3);
        fourmi = new Fourmi(null);
        fourmi.initialize(monde);
        //La fourmi spawn aux coordonn�es 1/1
        fourmi.deplacement(Direction.DOWN);
        assertEquals(fourmi.getPosH(), 2);
        fourmi.deplacement(Direction.DOWN);
        assertEquals(fourmi.getPosH(), 0);
    }

    @Test
    void testDeplacementLEFT() {
        monde = new Monde(3, 3);
        fourmi = new Fourmi(null);
        fourmi.initialize(monde);
        //La fourmi spawn aux coordonn�es 1/1
        fourmi.deplacement(Direction.LEFT);
        assertEquals(fourmi.getPosW(), 0);
        fourmi.deplacement(Direction.LEFT);
        assertEquals(fourmi.getPosW(), 2);
    }

    @Test
    void testDeplacementRIGHT() {
        monde = new Monde(3, 3);
        fourmi = new Fourmi(null);
        fourmi.initialize(monde);
        //La fourmi spawn aux coordonn�es 1/1
        fourmi.deplacement(Direction.RIGHT);
        assertEquals(fourmi.getPosW(), 2);
        fourmi.deplacement(Direction.RIGHT);
        assertEquals(fourmi.getPosW(), 0);
    }

    @Test
    void testDeposerNourritureFail() {
        monde = new Monde(3, 3);
        fourmi = new Fourmi(null);
        fourmi.initialize(monde);
        assertFalse(fourmi.doPorteNourriture() && fourmi.deposerNourriture());
    }

    @Test
    void testDeposerNourritureFourmiliere() {
        monde = new Monde(3, 3);
        monde.setCase(1, 1, new SourceDeNourriture(1));
        fourmi = new Fourmi(null);
        fourmi.initialize(monde);
        assertTrue(fourmi.ramasserNourriture());
        monde.setCase(1, 1, new Fourmiliere());
        Fourmiliere f = (Fourmiliere) monde.getCase(1, 1);
        assertTrue(fourmi.deposerNourriture());
        assertEquals(f.getCompteurNourriture(), 1);

    }

    @Test
    void testDeposerNourritureEmpty() {
        monde = new Monde(3, 3);
        monde.setCase(1, 1, new SourceDeNourriture(1));
        fourmi = new Fourmi(null);
        fourmi.initialize(monde);
        fourmi.ramasserNourriture();
        assertFalse(monde.getCase(1, 1) instanceof SourceDeNourriture);
        assertTrue(fourmi.deposerNourriture());
        assertTrue(monde.getCase(1, 1) instanceof SourceDeNourriture);
    }

    @Test
    void testRamasserNourritureFail() {
        monde = new Monde(3, 3);
        fourmi = new Fourmi(null);
        fourmi.initialize(monde);
        assertFalse(fourmi.ramasserNourriture());
    }

    @Test
    void testRamasserNourritureSource2() {
        monde = new Monde(3, 3);
        monde.setCase(1, 1, new SourceDeNourriture(2));
        fourmi = new Fourmi(null);
        fourmi.initialize(monde);

        assertTrue(monde.getCase(1, 1) instanceof SourceDeNourriture && !fourmi.doPorteNourriture() && fourmi.ramasserNourriture() && fourmi.doPorteNourriture());
    }

    @Test
    void testRamasserNourritureSource1() {
        monde = new Monde(3, 3);
        monde.setCase(1, 1, new SourceDeNourriture(1));
        fourmi = new Fourmi(null);
        fourmi.initialize(monde);

        assertTrue(monde.getCase(1, 1) instanceof SourceDeNourriture && !fourmi.doPorteNourriture() && fourmi.ramasserNourriture() && fourmi.doPorteNourriture() && !(monde.getCase(1, 1) instanceof SourceDeNourriture));
    }

    @Test
    void testRamasserNourritureFourmiliere() {
        monde = new Monde(3, 3);
        monde.setCase(1, 1, new Fourmiliere(1));
        fourmi = new Fourmi(null);
        fourmi.initialize(monde);
        assertFalse(fourmi.doPorteNourriture());
        assertFalse(fourmi.ramasserNourriture());
    }

}
