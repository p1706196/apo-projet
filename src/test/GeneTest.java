/**
 *
 */
package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import eugenisme.model.ant.Gene;
import eugenisme.model.ant.Gene.GeneType;

/**
 *
 */
class GeneTest {

    private Gene gene, gene2, gene3;

    /**
     * @throws java.lang.Exception
     */
    @BeforeAll
    static void setUpBeforeClass() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterAll
    static void tearDownAfterClass() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @BeforeEach
    void setUp() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterEach
    void tearDown() throws Exception {
        gene = null;
        gene2 = null;
        gene3 = null;
    }

    @Test
    void testEqualsSuccess() {
        gene = new Gene(GeneType.DETECTE_NOURRITURE, new Gene(GeneType.RAMASSER_NOURRITURE), new Gene(GeneType.DEPLACEMENT_HAUT));
        gene2 = new Gene(GeneType.DETECTE_NOURRITURE, new Gene(GeneType.RAMASSER_NOURRITURE), new Gene(GeneType.DEPLACEMENT_HAUT));

        assertTrue(gene.equals(gene2));
    }

    //Test d'un �quals lorsqu'une condition est diff�rente
    @Test
    void testEqualsFail1() {
        gene = new Gene(GeneType.DETECTE_NOURRITURE, new Gene(GeneType.RAMASSER_NOURRITURE), new Gene(GeneType.DEPLACEMENT_GAUCHE));
        gene2 = new Gene(GeneType.DETECTE_FOURMILIERE, new Gene(GeneType.RAMASSER_NOURRITURE), new Gene(GeneType.DEPLACEMENT_GAUCHE));

        assertFalse(gene.equals(gene2));
    }

    //Test d'un �quals lorsqu'une action est diff�rente
    @Test
    void testEqualsFail2() {
        gene = new Gene(GeneType.DETECTE_NOURRITURE, new Gene(GeneType.RAMASSER_NOURRITURE), new Gene(GeneType.DEPLACEMENT_GAUCHE));
        gene2 = new Gene(GeneType.DETECTE_NOURRITURE, new Gene(GeneType.RAMASSER_NOURRITURE), new Gene(GeneType.DEPLACEMENT_DROITE));

        assertFalse(gene.equals(gene2));
    }

    @Test
    void testToString() {
        gene = new Gene(GeneType.DETECTE_NOURRITURE, new Gene(GeneType.RAMASSER_NOURRITURE), new Gene(GeneType.DEPLACEMENT_HAUT));
        assertEquals(gene.toString(), "DETECTE_NOURRITURE[RAMASSER_NOURRITURE, DEPLACEMENT_HAUT]");
    }

    @Test
    void testFromString() {
        gene = Gene.fromString("DETECTE_NOURRITURE[RAMASSER_NOURRITURE, DEPLACEMENT_HAUT]");
        assertEquals(gene.getType(), GeneType.DETECTE_NOURRITURE);
        assertEquals(gene.getSousGeneById(1).getType(), GeneType.RAMASSER_NOURRITURE);
        assertEquals(gene.getSousGeneById(2).getType(), GeneType.DEPLACEMENT_HAUT);
    }

    @Test
    void testIsRootSuccess() {
        gene = new Gene(GeneType.DETECTE_NOURRITURE, new Gene(GeneType.RAMASSER_NOURRITURE), new Gene(GeneType.DEPLACEMENT_HAUT));
        assertTrue(gene.isRoot());
    }

    @Test
    void testIsRootFail() {
        gene = new Gene(GeneType.DETECTE_NOURRITURE, new Gene(GeneType.RAMASSER_NOURRITURE), new Gene(GeneType.DEPLACEMENT_HAUT));
        assertFalse(gene.getSousGeneById(1).isRoot());
    }

    //Test de clean lorsqu'il n'est pas n�cessaire de faire des changements
    @Test
    void testClean1() {
        gene = new Gene(GeneType.DETECTE_NOURRITURE, new Gene(GeneType.RAMASSER_NOURRITURE), new Gene(GeneType.DEPLACEMENT_HAUT));
        gene2 = new Gene(GeneType.DETECTE_NOURRITURE, new Gene(GeneType.RAMASSER_NOURRITURE), new Gene(GeneType.DEPLACEMENT_HAUT));
        gene.clean();
        assertTrue(gene.equals(gene2));
    }

    //Test de clean lorsqu'une condition a un fils avec la m�me condition
    @Test
    void testClean2() {
        gene = new Gene(GeneType.DETECTE_NOURRITURE,
                new Gene(GeneType.DETECTE_NOURRITURE, new Gene(GeneType.RAMASSER_NOURRITURE), new Gene(GeneType.DEPLACEMENT_HAUT))
                , new Gene(GeneType.DEPLACEMENT_HAUT));

        gene2 = new Gene(GeneType.DETECTE_NOURRITURE, new Gene(GeneType.RAMASSER_NOURRITURE), new Gene(GeneType.DEPLACEMENT_HAUT));

        gene.clean();
        assertTrue(gene.equals(gene2));
    }

    //Test de clean lorsque ses deux fils sont la m�me action
    @Test
    void testClean3() {
        gene = new Gene(GeneType.DETECTE_NOURRITURE,
                new Gene(GeneType.PORTE_NOURRITURE, new Gene(GeneType.DEPLACEMENT_DROITE), new Gene(GeneType.DEPLACEMENT_DROITE))
                , new Gene(GeneType.DEPLACEMENT_HAUT));
        gene.clean();

        assertFalse(gene.getSousGeneById(2).equals(gene.getSousGeneById(3)));

    }


    @Test
    void testCopy() {
        gene = new Gene(GeneType.DETECTE_NOURRITURE, new Gene(GeneType.RAMASSER_NOURRITURE), new Gene(GeneType.DEPLACEMENT_HAUT));
        gene2 = new Gene();
        gene2.copy(gene);

        assertTrue(gene.equals(gene2));
    }

    //Comment tester de l'al�atoire?
    @Test
    void testBreed() {
        gene = new Gene(GeneType.DETECTE_NOURRITURE, new Gene(GeneType.RAMASSER_NOURRITURE), new Gene(GeneType.DEPLACEMENT_HAUT));
        gene2 = new Gene(GeneType.DETECTE_FOURMILIERE, new Gene(GeneType.DEPLACEMENT_DROITE), new Gene(GeneType.DEPLACEMENT_GAUCHE));
        gene3 = new Gene(GeneType.DETECTE_NOURRITURE, new Gene(GeneType.RAMASSER_NOURRITURE), new Gene(GeneType.DEPLACEMENT_HAUT));

        gene.breed(gene2);

        assertFalse(gene.equals(gene2) || gene.equals(gene3));
    }


    @Test
    void testForceMutation() {
        gene = new Gene(GeneType.DETECTE_NOURRITURE, new Gene(GeneType.RAMASSER_NOURRITURE), new Gene(GeneType.DEPLACEMENT_HAUT));
        gene2 = new Gene(GeneType.DETECTE_NOURRITURE, new Gene(GeneType.RAMASSER_NOURRITURE), new Gene(GeneType.DEPLACEMENT_HAUT));

        gene.forceMutation();
        assertFalse(gene.equals(gene2));


    }

}
