package eugenisme.view.uigene;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JFrame;

import eugenisme.controller.Eugenisme;
import eugenisme.model.Experience;

/**
 * Affiche l'arbre g�n�tique final de la simulation dans une fen�tre
 */

public class UIGene extends JFrame {
    private static final long serialVersionUID = 3494763922913440195L;
    Experience exp;
    UIGenePanel panel;

    /**
     * Constructeur de la fen�tre
     *
     * @param exp l'exp�rience actuelle
     */
    public UIGene(Experience exp) {
        this.exp = exp;
        this.setTitle("Arbre genome");
        this.setSize(1200, 1000);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
        panel = new UIGenePanel(exp); // on cree le Canvas
        this.add(panel);
        List<String> listeFourmis = new ArrayList<String>(); // liste des fourmis
        for (int i = 1; i <= exp.tailleSelectionTopFourmi.get(); i++) {
            listeFourmis.add("Fourmis " + i);
        }// on s�lectionne les meilleures fourmis d�finies par tailleSelectionTopFourmi

        JComboBox choixFourmis = new JComboBox(); // on met les fourmis dans une combobox
        for (Object o : listeFourmis) {
            choixFourmis.addItem(o);
        }
        choixFourmis.setPreferredSize(new Dimension(100, 50));
        this.add(choixFourmis, BorderLayout.SOUTH);
        ActionListener actionListener = new ActionListener() { // quand la combobox change d'index
            public void actionPerformed(ActionEvent actionEvent) {
                addPanel(actionEvent, choixFourmis); // on recree le Canvas
            }
        };
        choixFourmis.addActionListener(actionListener);
        
        // ref permet de passer la référence dans l'event handler
        JFrame ref = this;
        ref.addWindowListener(new WindowAdapter() {
        	public void windowClosing(WindowEvent windowEvent) {
        		ref.setVisible(false);
        		Eugenisme.stopDisplayFourmi();
        	}
		});
    }

    private void addPanel(ActionEvent e, JComboBox<String> choixFourmis) {
        this.remove(panel);
        panel = new UIGenePanel(exp.getFourmis().get(choixFourmis.getSelectedIndex()).getGenome());
        this.add(panel);
        this.revalidate();
        this.repaint();
    }


}
