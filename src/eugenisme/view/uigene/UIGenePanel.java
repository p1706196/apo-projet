package eugenisme.view.uigene;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import eugenisme.model.Experience;
import eugenisme.model.ant.Gene;

/**
 * Composant Canvas
 * (Dessin de l'arbre)
 */
public class UIGenePanel extends Canvas {

    private static final long serialVersionUID = -819073028459637117L;
    private Gene genome;
    private int baseX = 600;
    private int coeff = 15;
    private static final int FONT_SIZE = 12;
    private String[] sGene;

    /**
     * Constructeur avec les résultats d'une expérience
     *
     * @param exp l'expérience actuelle
     */
    public UIGenePanel(Experience exp) {
        this.genome = exp.getFourmis().get(0).getGenome();
    }

    /**
     * Constructeur quand gene est connu
     *
     * @param gene
     */

    public UIGenePanel(Gene gene) {
        this.genome = gene;
    }


    public void paint(Graphics g) {
        super.paint(g);
        baseX = (g.getClipBounds().width - g.getClipBounds().x)/2;
        paintGenome(g, genome, baseX, 40, Color.BLACK);
    }

    /**
     * Parcourt et draw les noeuds et branches
     *
     * @param gene
     * @param g
     */
    public void paintGenome(Graphics g, Gene gene, int x, int y, Color c) {
        g.setColor(c);
        g.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, FONT_SIZE));
        sGene = gene.getType().toString().split("_");

        for (int i = 0, j = 0; j < sGene.length; i += 15, j++)
            g.drawString(sGene[j], x, y + i);

        if (gene.isNode()) {
            g.setColor(Color.BLUE);
            g.drawLine(x + 50, y + 20, x - (coeff * gene.getHeight() - 10), y + 50);
            g.setColor(Color.RED);
            g.drawLine(x + 50, y + 20, x + (coeff * gene.getHeight() + 90), y + 50);

            paintGenome(g, gene.getNext(true), x - (coeff * gene.getHeight() + 30), y + 60, Color.BLUE);//gauche
            paintGenome(g, gene.getNext(false), x + (coeff * gene.getHeight() + 60), y + 60, Color.RED);//droite
        }
    }

}
