package eugenisme.view;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Composant graphique servant à afficher une image
 */
public class ImagePanel extends JPanel {
    private static final long serialVersionUID = 4127998497644170733L;
    private BufferedImage img;

    public ImagePanel(String path) {
        try {
            img = ImageIO.read(new File(path));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(img, 0, 0, null);
    }
}
