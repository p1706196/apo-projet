package eugenisme.view;

import java.awt.GridLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JPanel;

import eugenisme.controller.Eugenisme;
import eugenisme.model.ant.Fourmi;
import eugenisme.model.world.Monde;

/**
 * Affiche l'évolution d'une fourmi dans le monde grâce à une fenêtre
 */
public class GUI {
    private JFrame frame;
    private JPanel panelPrincipal;
    private JPanel[][] panels;
    private Fourmi fourmi;
    private Monde monde;

    /**
     * Constructeur de la fenêtre
     *
     * @param monde  monde actuel à afficher
     * @param fourmi fourmi à afficher
     */
    public GUI(Monde monde, Fourmi fourmi) {
        this.monde = monde;
        this.fourmi = fourmi;
        this.panels = new JPanel[monde.getWidth()][monde.getHeight()];
    }

    /**
     * Créer la fenêtre à partir des paramètres de la classe
     */
    public void startUI() {
        frame = new JFrame();
        frame.setSize(100 * monde.getWidth(), 100 * monde.getHeight());
        
        frame.addWindowListener(new WindowAdapter() {
        	public void windowClosing(WindowEvent windowEvent) {
        		frame.setVisible(false);
        		Eugenisme.stopDisplayMonde();
        	}
		});

        panelPrincipal = new JPanel();
        panelPrincipal.setLayout(new GridLayout(monde.getWidth(), monde.getHeight()));
        panelPrincipal.setSize(100 * monde.getWidth(), 100 * monde.getHeight());


        frame.add(panelPrincipal);

        frame.setVisible(true);
    }


    /**
     * Boucle d'affichage de l'évolution de la fourmi dans le monde
     */
    public void updateUI() {

        panelPrincipal = new JPanel();
        panelPrincipal.setLayout(new GridLayout(monde.getWidth(), monde.getHeight()));

        for (int i = 0; i < monde.getWidth(); i++) {
            for (int j = 0; j < monde.getHeight(); j++) {

                if (fourmi.getPosH() == j && fourmi.getPosW() == i) {
                    panels[i][j] = new ImagePanel("ressources/images/fourmi.jpg");
                    panels[i][j].setSize(100, 100);
                } else {
                    if (monde.getCase(j, i).toString().startsWith("f")) {
                        panels[i][j] = new ImagePanel("ressources/images/fourmiliere.jpg");
                        panels[i][j].setSize(100, 100);
                    } else if (monde.getCase(j, i).toString().startsWith("n")) {
                        panels[i][j] = new ImagePanel("ressources/images/source.jpg");
                        panels[i][j].setSize(100, 100);
                    } else {
                        panels[i][j] = new JPanel();
                        panels[i][j].setSize(100, 100);
                    }
                }

                panelPrincipal.add(panels[i][j]);
            }
        }

        frame.add(panelPrincipal);

        frame.repaint();
        frame.setVisible(true);
    }

}
