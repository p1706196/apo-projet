package eugenisme.controller;

import java.io.File;

import eugenisme.model.Experience;
import eugenisme.model.InitializationFailException;
import eugenisme.model.SimulationManagerGUI;
import eugenisme.model.ant.Fourmi;
import eugenisme.model.world.Monde;
import eugenisme.utils.command.Command;
import eugenisme.utils.command.Line;
import eugenisme.utils.field.Field;
import eugenisme.view.uigene.UIGene;


/**
 * Eugenisme fait la gestion des expériences
 */
public class Eugenisme
{
	private static boolean wasPaused = false;
	
	private static final String SETTINGS_FILE_PATH = "ressources/config.txt";
	private volatile static State state = State.INITIALIZE;
	private static Experience experience  = new Experience();
	private static Thread threadEvent;
	
	private static Thread threadDisplayFourmi;
	private static Thread threadDisplayMonde;
	
	private static Fourmi displayFourmi = null;
	private static Monde displayMonde = null;
	
    /**
     * initilisation générale
     */
    private static void initialize() {
        // init was paused
        File pfile = new File(Experience.TMP_FILE_PAUSED);
        wasPaused = pfile.exists();
        if (wasPaused) System.out.println("Une opération était en pause");

        // goto next state
        state = State.SETUP;
    }

    /**
     * boucle générale
     */
    private static void mainLoop() {
        // affichage de l'avancement
        if (experience.getNbCompletedGen() != 0 && experience.getNbCompletedGen() % (experience.nbGen.get() / 100 == 0 ? 1 : experience.nbGen.get() / 100) == 0) {
            System.out.println(
                    experience.getNbCompletedGen() + "/" + experience.nbGen.get() + " | " +
                            experience.getNbCompletedGen() * 100 / experience.nbGen.get() +
                            "% | ETA " +
                            ((long) (
                                    experience.getElapsedTime() * (1. - experience.getNbCompletedGen() / experience.nbGen.get()) / experience.getNbCompletedGen() * experience.nbGen.get()
                                            - experience.getElapsedTime()
                            )) / 1000. +
                            "s | min " + experience.getScoreMin() +
                            " | moy " + experience.getScoreMoy() +
                            " | max " + experience.getScoreMax());
        }

		// On appelle itererGeneration
    	if(!experience.itererGeneration()) {
    		// si l'experience est fini on sauvegarde et on arrete la boucle
    		save();
    		if(experience.afficherFenetreGenesFourmi.get()) {
    			startDisplayFourmi();
    		}
    		if(experience.afficherFenetreSimulation.get()) {
    			displayFourmi = experience.getFourmis().get(0);
    			displayMonde = experience.getMonde().clone();
    			startDisplayMonde();
    		}
    		state = State.SETUP;
    	}
    }

    /**
     * Lance une fenêtre d'UI qui affiche les gènes des fourmis
     */
    private static void displayFourmi() {
        new UIGene(experience);
    }

    /**
     * Lance une fenêtre d'UI qui affiche le monde et la fourmi passés en paramètre.
     */
    private static void displayMonde() {
    	if(displayFourmi == null || displayMonde == null) {
    		System.out.println("Situation invalide");
    		return;
    	}
    	SimulationManagerGUI mondeGUI =
				new SimulationManagerGUI(displayMonde,
						experience.tempsSim.get(),
						displayFourmi);
		mondeGUI.run();
    }	

    /**
     * sauvegarde
     */
    private static void save() {
        //sauvegarde
        experience.save();
        System.out.println("done (" + experience.getElapsedTime() / 1000. + "s)");

        experience.invalidate();
    }

    /**
     * sauvegarde l'état de l'experience dans des fichiers "cache" pour pouvoir resume
     */
    private static void pause() {
        //sauvegarde
        experience.pauseToFile();
        wasPaused = true;
        System.out.println("paused");

        experience.invalidate();
    }

    /**
     * sortie principale
     */
    private static void exit() {
        System.out.println("exited");

        state = State.KILL;
    }

    /**
     * @param args arguments de la ligne de commande
     */
    public static void main(String[] args) {
        // remise des arguments en ligne
        String input = "";
        for (String w : args) {
            input += w + " ";
        }
        // chargement des arguments dans le queue des inputs
        InputHandler.parse(input);
        //lancement du thread d'input
        InputHandler.start();

        // init thread event
        threadEvent = new Thread(Eugenisme::eventThread);
        threadEvent.setDaemon(true);
        // lancement traitement des inputs
        threadEvent.start();
        System.out.println("Entrez \"start\" ou \"quickstart\" pour débuter la simulation, ou \"help\" pour avoir la liste des commandes");
        
        // boucle générale
        while (state != State.KILL) {
            state.run();
        }

        // fin gestion des inputs
        threadEvent.interrupt();
        // fin lecture des inputs
        InputHandler.interrupt();
    }

    /**
     * thread parallèle permettant de traiter les entrées utilisateur
     */
    private static void eventThread() {
    	Line cmd;
        while (state != State.KILL) {
        	cmd = InputHandler.nextLine(); 
        	state.execute(cmd);
        }
    }
    
    /**
     * Démare le Thread qui encapsule l'affichage du génome des fourmis
     */
    public static void startDisplayFourmi() {
    	stopDisplayFourmi();
    	
        threadDisplayFourmi = new Thread(Eugenisme::displayFourmi);
        threadDisplayFourmi.setDaemon(true);
    	
    	threadDisplayFourmi.start();
    }
    
    /**
     * Démare le Thread qui encapsule l'affichage du Monde et de la fourmi qui y vit
     */
    public static void startDisplayMonde() {
    	stopDisplayMonde();
    	
        threadDisplayMonde = new Thread(Eugenisme::displayMonde);
        threadDisplayMonde.setDaemon(true);
        
        threadDisplayMonde.start();
    }
    
    /**
     * Arrête le Thread d'affichage du génome
     */
    public static void stopDisplayFourmi() {
    	if(threadDisplayFourmi != null && threadDisplayFourmi.isAlive())
    		threadDisplayFourmi.interrupt();
	}
    
    /**
     * Arrête le Thread d'affichage du Monde
     */
    public static void stopDisplayMonde() {
    	if(threadDisplayMonde != null && threadDisplayMonde.isAlive())
    		threadDisplayMonde.interrupt();
	}

    /**
	 * liste des etats du programme
	 * @author adrien
	 *
	 */
	public enum State
	{
		// initialisation generale
		INITIALIZE(Eugenisme::initialize, null),
		// phase de setup
		SETUP(null, SETUP_COMMAND()),
		RUN(Eugenisme::mainLoop, RUN_COMMAND()),
		KILL(null, null);
		
		private Runnable function;
		private Command command;
		private State(Runnable func, Command cmd) {function = func; command = cmd; }
		public void run() { if(function != null) function.run(); }
		public void execute(Line cmd) { if(command != null) command.execute(cmd); }
	
    
	    /**
	     * Commande pour la phase de setup
	     * les commandes sont donnees par des fonction et non par des constantes car les constantes sont initialisees apres les enum en java, donc unitilisable ici
	     * le but est aussi de deplacer l'initialisation car elle est massive et rendrais le code ilisible
	     */
		private static Command SETUP_COMMAND()
		{
			return new Command( line -> InputHandler.parse("help") )
			.addSubCommand("config", 
					new Command( line -> InputHandler.parse("config help") )
					.addSubCommand("help", 
							new Command( line -> System.out.println("config <help/set/get/list/load/save>") )
					).addSubCommand("list", 
							new Command(line -> 
							{ 
								System.out.println("Paramètres disponibles : ");
								for(Field<?> field : experience.fieldCluster.getFields())
								{
									System.out.println("- " + field.getName() + " : " + field.get());
								}
							}
							)
					).addSubCommand("set", 
							new Command(line -> 
							{
								String name = line.next();
								String arg = line.next();
								if(name == null || arg == null)
								{
									System.out.println("config set <name> <value>");
									return;
								}
								try
								{
									experience.fieldCluster.parseField(name, arg);
									System.out.println(name + " set to " + arg);
								}
								catch(NullPointerException e)
								{
									System.out.println("Setting not found : " + name);
								}
								catch(NumberFormatException e)
								{
									System.out.println("Invalid parametter : " + arg);
								}
							}
							)
					).addSubCommand("get", 
							new Command(line -> 
							{
								String name = line.next();
								if(name == null)
								{
									System.out.println("config get <name>");
									return;
								}
								Field<?> field = experience.fieldCluster.getField(name);
								if(field != null)
									System.out.println(field.getName() + " : " + field.get());
								else	
									System.out.println("Setting not found : " + name);
							}
							)
					).addSubCommand("load", 
							new Command(line -> InputHandler.parse("config load help"))
							.addSubCommand("default", 
									new Command(line -> 
									{ 
										System.out.println("loading settings from : " + SETTINGS_FILE_PATH);
										experience.loadSettingsFromFile(SETTINGS_FILE_PATH); 
									}
									)
							).addSubCommand("path", 
									new Command(line -> 
									{ 
										String path = line.next();
										if(path == null)
										{
											System.out.println("config load path <path>");
											return;
										}
										System.out.println("loading settings from : " + path);
										experience.loadSettingsFromFile(path); 
									}
									)
							).addSubCommand("help", 
									new Command( line -> System.out.println("config load <help/default/path>") )
							)
					).addSubCommand("save", 
							new Command( line -> InputHandler.parse("config save help") )
							.addSubCommand("default", 
									new Command(line -> 
									{ 
										System.out.println("saving settings to : " + SETTINGS_FILE_PATH);
										experience.saveSettingsToFile(SETTINGS_FILE_PATH);
									}
									)
							).addSubCommand("path", 
									new Command(line -> 
									{ 
										String path = line.next();
										if(path == null)
										{
											System.out.println("config save path <path>");
											return;
										}
										System.out.println("saving settings to : " + path);
										experience.saveSettingsToFile(path); 
									}
									)
							).addSubCommand("help", 
									new Command( line -> System.out.println("config save <help/default/path>") )
							)
					)
			).addSubCommand("display", 
					new Command(line -> InputHandler.parse("display help") )
					.addSubCommand("help", 
							new Command(line -> System.out.println("display <help/select/show/hide>") )
					).addSubCommand("select", 
							new Command(line -> InputHandler.parse("display select help") )
							.addSubCommand("help", 
									new Command(line -> System.out.println("display select <help/fourmi/monde>") )
							).addSubCommand("fourmi", 
									new Command(line -> InputHandler.parse("display select fourmi help") )
									.addSubCommand("help", 
											new Command(line -> System.out.println("display select fourmi <help/fromString/fromExperience>") )
									).addSubCommand("fromString", 
											new Command(line ->
											{
												if(line.isEmpty())
												{
													System.out.println("display select fourmi fromString <fourmi>");
													return;
												}
												try { displayFourmi = Fourmi.fromString(line.dump()); } 
												catch(IllegalArgumentException e) {
													System.out.println("genome invalide");
													displayFourmi = null;
												}
												if(displayFourmi != null)
													System.out.println("Fourmi chargée");
											}
											)
									).addSubCommand("fromExperience", 
											new Command(line ->
											{
												if(experience.getFourmis().isEmpty())
												{
													System.out.println("L'experience n'a pas donné de résultat");
													return;
												}
												if(line.isEmpty())
												{
													System.out.println("display select fourmi fromExperience <id>");
													return;
												}
												String arg = line.next();
												int id = 0;
												try { id = Integer.parseInt(arg); }
												catch (NumberFormatException e) {
													System.out.println("Invalid parametter : " + arg);
													return;
												}
												if(experience.tailleSelectionTopFourmi.get() < id && id >= 0)
												{
													displayFourmi = experience.getFourmis().get(id);
													System.out.println("Fourmi chargée");
												}
												else
												{
													System.out.println("id invalide");
												}
											}
											)
									)
							).addSubCommand("monde", 
									new Command(line -> InputHandler.parse("display select monde help") )
									.addSubCommand("help", 
											new Command(line -> System.out.println("display select fourmi <help/fromFile/fromExperience>") )
									).addSubCommand("fromFile", 
											new Command(line ->
											{
												int h;
												int w;
												String path;
												try {
													if(line.isEmpty()) { System.out.println("display select monde fromFile <height> <width> <path>"); return; }
													h = Integer.parseInt(line.next());
													if(line.isEmpty()) { System.out.println("display select monde fromFile <height> <width> <path>"); return; }
													w = Integer.parseInt(line.next());
													if(line.isEmpty()) { System.out.println("display select monde fromFile <height> <width> <path>"); return; }
													path = line.next();
												} catch (NullPointerException e) {
													System.out.println("display select monde fromFile <height> <width> <path>");
													return;
												} catch (NumberFormatException e) {
													System.out.println("Invalid parametter : the two first parametters must be integers");
													return;
												}
												displayMonde = new Monde(h, w);
												displayMonde.loadFromFile(path);
											}
											)
									).addSubCommand("fromExperience", 
											new Command(line ->
											{
												Monde md = experience.getMonde();
												if(md == null)
												{
													System.out.println("Experience n'a pas de monde chargé");
													return;
												}
												displayMonde = md.clone();
											}
											)
									)
							)
					).addSubCommand("show", 
							new Command(line -> InputHandler.parse("display show help") )
							.addSubCommand("help", 
									new Command(line -> System.out.println("display show <help/genome/simulation>") )
							).addSubCommand("genome", 
									new Command(line -> 
									{
										if(experience.getFourmis().isEmpty())
										{
											System.out.println("Rien à afficher : l'experience n'a jamais donné de résultat");
											return;
										}
										startDisplayFourmi();
									}
									)
							).addSubCommand("simulation", 
									new Command(line ->
										startDisplayMonde()
									)
							)
					).addSubCommand("hide", 
							new Command(line -> InputHandler.parse("display hide help") )
							.addSubCommand("help", 
									new Command(line -> System.out.println("display hide <help/genome/simulation>") )
							).addSubCommand("genome", 
									new Command(line ->
									{
										stopDisplayFourmi();
										System.out.println("Genome caché");
									}
									)
							).addSubCommand("simulation", 
									new Command(line ->
									{
										stopDisplayMonde();
										System.out.println("Simulation cachée");
									}
									)
							)
					)
			).addSubCommand("resume", 
					new Command(line -> 
					{
						if(!wasPaused)
						{
							System.out.println("Initialization  failed : Aucune oppération n'est en pause");
							return;
						}
						try 
						{ 
							experience.resumeFromFile();
							state = State.RUN;
							System.out.println("resumed from pause"); 
						} 
						catch (InitializationFailException e) 
						{
							System.out.println("Initialization  failed : " + e.getMessage()); 
							return;
						}
					}
					)
			).addSubCommand("start", 
					new Command(line -> 
					{
						try 
						{ 
							experience.initialize();
							state = State.RUN;
							System.out.println("begin"); 
						} 
						catch (InitializationFailException e) 
						{
							System.out.println("Initialization  failed : " + e.getMessage()); 
							return;
						}
					}
					)
			).addSubCommand("exit", 
					new Command(line -> 
						exit()
					)
			).addSubCommand("quickstart", 
					new Command(line -> InputHandler.parse("config load default; start") )
			).addSubCommand("help", 
					new Command(line -> System.out.println("<help/config/resume/start/quickstart/exit/display>") )
			);
	
		}
		/**
	     * Commande pour la phase de run
	     */
		private static Command RUN_COMMAND() 
		{
			return new Command(line -> InputHandler.parse("help") )
			.addSubCommand("help",
					new Command(line -> System.out.println("<help/exit/save/pause>") )	
			).addSubCommand("exit",
					new Command(line -> 
						exit()
					)	
			).addSubCommand("save",
					new Command(line -> 
					{
						save();
						state = State.SETUP;
					}
					)	
			).addSubCommand("pause",
					new Command(line -> 
					{
						pause();
						state = State.SETUP;
					}
					)	
			);
		}
	}
}
