package eugenisme.controller;

import java.util.Scanner;
import java.util.concurrent.ConcurrentLinkedQueue;

import eugenisme.utils.command.Line;

/**
 * classe utilitaire pour gérer les entrées de l'utilisateur
 * cette classe est statique
 */
public class InputHandler {
    private static ConcurrentLinkedQueue<Line> input = new ConcurrentLinkedQueue<Line>();
    private static Thread thread;
    private static boolean running = false;

    /**
     * construction statique
     */
    static {
        thread = new Thread(InputHandler::inputThread);
        thread.setDaemon(true);
    }

    /**
     * démarre le thread associé a la lecture des entrées utilisateur
     */
    public static void start() {
        if (running) return;
        running = true;
        thread.start();
    }

    /**
     * interrompt le thread de lecture
     */
    public static void interrupt() {
        running = false;
        thread.interrupt();
    }

    /**
     * attend la fin du thread de lecture
     *
     * @param millis le temps à attendre en millisecondes
     */
    public static void join(long millis) {
        if (!running) return;
        running = false;
        input.offer(new Line());
        input.notifyAll();
        try {
            thread.join(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * la prochaine entrée utilisateur, fonction bloquante
     *
     * @return un mot
     * @throws InterruptedException si le programme se termine alors que cette fonction est en train d'attendre
     */
    public static Line nextLine() {
        while (input.isEmpty()) try {
            synchronized (input) {
                input.wait();
            }
        } catch (InterruptedException e) {
        }
        return input.poll();

    }

    /**
     * permet d'ajouter artificiellement des entrées utilisateur
     *
     * @param lines la ou les entrées à ajouter
     */
    public static void addInput(Line... lines) {
        for (Line element : lines) {
            input.offer(element);
            synchronized (input) {
                input.notify();
            }
        }
    }

    /**
     * interprète la ligne donnée et l'ajoute à l'input
     *
     * @param data
     */
    public static void parse(String data) {
        for (String cmd : data.split(";")) {
            Line line = new Line(cmd.split("[ \t]"));
            if (!line.isEmpty()) {
                input.offer(line);
                synchronized (input) {
                    input.notify();
                }
            }
        }
    }

    /**
     * la fonction du thread associé
     */
    private static void inputThread() {
        Scanner sc = new Scanner(System.in);
        while (running) {
            System.out.print("> ");
            String line = sc.nextLine();
            parse(line);
        }
        sc.close();
    }
}
