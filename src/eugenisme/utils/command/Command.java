package eugenisme.utils.command;

import java.util.TreeMap;

/**
 * Classe utilitaire pour construire des commandes
 */
public class Command {
    private TreeMap<String, Command> subCommands = new TreeMap<String, Command>();
    private CommandAction func = null;

    /**
     * Constructeur pour une commande sans action
     */
    public Command() {
        this(null);
    }

    /**
     * Constucteur pour une commande runnable
     *
     * @param action l'action faite par la commande
     */
    public Command(CommandAction action) {
        func = action;
    }

    /**
     * Permet d'ajouter une sous commande
     *
     * @param name la nom de la sous commande
     * @param cmd  la sous commande
     * @return this pour pouvoir enchaîner
     */
    public Command addSubCommand(String name, Command cmd) {
        subCommands.put(name, cmd);
        return this;
    }

    /**
     * Donne la sous commande correspondant au nom donné
     *
     * @param name le nom de la sous commande
     * @return la sous commande ou null si aucune n'est trouvée, si la chaîne est vide retourne this
     */
    public Command getSubCommand(String name) {
        if (name.isEmpty()) return this;
        return subCommands.get(name);
    }

    /**
     * exécute la commande en fonction de la ligne donnée par l'utilisateur
     *
     * @param line la ligne à interpréter
     * @return une sous commande runnable ou null si la fin de la chaine à été atteinte sans trouver de commande runnable
     */
    public void execute(Line line) {
        if (this.hasSubCommand() && line.hasNext()) {
            String id = line.next();
            Command next = this.getSubCommand(id);
            if (next != null) {
                next.execute(line);
                return; // sortie normale
            }
        }
        this.run(line); //si un des if ne passe pas on fini ici
    }

    /**
     * exécute la fonction associée si elle existe
     *
     * @param line les paramàtres a donner à la fonction
     */
    public void run(Line line) {
        if (func != null) func.run(line);
    }

    public boolean hasSubCommand() {
        return !subCommands.isEmpty();
    }
}
