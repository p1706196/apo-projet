package eugenisme.utils.command;

import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Classe représentant une ligne
 */
public class Line {
    private ConcurrentLinkedQueue<String> line = new ConcurrentLinkedQueue<String>();

    /**
     * Construit la ligne
     *
     * @param words les mots à mettre dans la ligne
     */
    public Line(String... words) {
        addWord(words);
    }

    /**
     * Ajoute des mots dans le ligne
     *
     * @param strings les mots
     */
    public void addWord(String... strings) {
        for (String element : strings) {
            if (!element.trim().isEmpty())
                line.offer(element.trim());
        }
    }

    /**
     * Retourne le prochain mot et le retire de la queue
     *
     * @return le mot suivant, null si la ligne est vide
     */
    public String next() {
        return line.poll();
    }

    /**
     * Retourne le prochain mot sans affecter la queue
     *
     * @return le mot suivant, null si la ligne est vide
     */
    public String peek() {
        return line.peek();
    }
	
	/**
	 * Retourne tout le reste de la ligne, apres la fonction la ligne est vide
	 *
	 * @return les mots restants separes par un espace
	 */
	public String dump()
	{
		String ret = "";
		while(!line.isEmpty())
			ret += line.poll() + " ";
		return ret;
	}

    public boolean hasNext() {
        return !line.isEmpty();
    }

    public boolean isEmpty() {
        return line.isEmpty();
    }
}
