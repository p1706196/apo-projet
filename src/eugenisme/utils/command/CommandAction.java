package eugenisme.utils.command;

public interface CommandAction {
    public void run(Line line);
}
