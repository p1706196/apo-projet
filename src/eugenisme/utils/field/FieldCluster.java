package eugenisme.utils.field;

import java.util.Iterator;
import java.util.Map.Entry;
import java.util.TreeMap;

import eugenisme.utils.Fichier;

/**
 * Un essemble de champs, permet également leur construction
 */
public class FieldCluster {
    private TreeMap<String, Field<?>> fields = new TreeMap<String, Field<?>>();
    private boolean locked = false;

    /**
     * Constructeur vide
     */
    public FieldCluster() {
    }

    /**
     * Constucteur générique pour les champs
     *
     * @param <T>   type
     * @param name  nom
     * @param value valeur
     * @return le champ construit
     */
    private <T> Field<T> _getNewField(String name, T value) {
        Field<T> field = new Field<T>(this, name, value);
        fields.put(name, field);
        return field;

    }

    /**
     * Construit un Field<Boolean> associé au cluster
     *
     * @param name  le nom du champ, il doit être unique
     * @param value la valeur du champ
     * @return le champ construit
     */
    public Field<Boolean> getNewField(String name, boolean value) {
        return _getNewField(name, value);
    }

    /**
     * Construit un Field<Double> associé au cluster
     *
     * @param name  le nom du champ, il doit être unique
     * @param value la valeur du champ
     * @return le champ construit
     */
    public Field<Double> getNewField(String name, double value) {
        return _getNewField(name, value);
    }

    /**
     * Construit un Field<Integer> associé au cluster
     *
     * @param name  le nom du champ, il doit être unique
     * @param value la valeur du champ
     * @return le champ construit
     */
    public Field<Integer> getNewField(String name, int value) {
        return _getNewField(name, value);
    }

    /**
     * Construit un Field<String> associé au cluster
     *
     * @param name  le nom du champ, il doit être unique
     * @param value la valeur du champ
     * @return le champ construit
     */
    public Field<String> getNewField(String name, String value) {
        return _getNewField(name, value);
    }

    /**
     * Sauvegarde les champs en charge dans le fichier donné
     *
     * @param path le chemin vers le fichier
     */
    public void saveToFile(String path) {
        Fichier file = new Fichier();
        Iterator<Entry<String, Field<?>>> it = fields.entrySet().iterator();
        while (it.hasNext()) {
            Entry<String, Field<?>> obj = it.next();
            if (obj.getValue().get() instanceof Boolean) {
                file.setBoolean(obj.getKey(), (Boolean) obj.getValue().get());
            } else if (obj.getValue().get() instanceof Double) {
                file.setDouble(obj.getKey(), (Double) obj.getValue().get());
            } else if (obj.getValue().get() instanceof Integer) {
                file.setInteger(obj.getKey(), (Integer) obj.getValue().get());
            } else if (obj.getValue().get() instanceof String) {
                file.setString(obj.getKey(), (String) obj.getValue().get());
            }

        }
        file.saveToFile(path);
    }

    /**
     * Charge les champs dans le fichier donné
     *
     * @param path le chemin vers le fichier
     * @throws FieldLockedException si le cluster est verrouillé
     */
    @SuppressWarnings("unchecked")
    public void loadFromFile(String path) throws FieldLockedException {
        if (locked) throw new FieldLockedException();
        Fichier file = new Fichier();
        if (file.loadFromFile(path)) {
            Iterator<Entry<String, Field<?>>> it = fields.entrySet().iterator();
            while (it.hasNext()) {
                Entry<String, Field<?>> obj = it.next();
                if (obj.getValue().get() instanceof Boolean) {
                    ((Field<Boolean>) obj.getValue()).set(file.getBooleanOrDefault(obj.getKey(), (Boolean) obj.getValue().get()));
                } else if (obj.getValue().get() instanceof Double) {
                    ((Field<Double>) obj.getValue()).set(file.getDoubleOrDefault(obj.getKey(), (Double) obj.getValue().get()));
                } else if (obj.getValue().get() instanceof Integer) {
                    ((Field<Integer>) obj.getValue()).set(file.getIntegerOrDefault(obj.getKey(), (Integer) obj.getValue().get()));
                } else if (obj.getValue().get() instanceof String) {
                    ((Field<String>) obj.getValue()).set(file.getStringOrDefault(obj.getKey(), (String) obj.getValue().get()));
                }

            }
        }
    }

    /**
     * @param name le nom du champ à recupérer
     * @return le champ désigné par ce nom, null si aucun champ ne correspond
     */
    public Field<?> getField(String name) {
        return fields.get(name);
    }

    /**
     * @param name le nom du champ a modifier
     * @param str  la valeur à associer au champ
     * @throws NumberFormatException si le champ est un nombre et que la valeur donnée n'est pas un nombre du bon format
     * @throws NullPointerException  si aucun champ avec le nom donne n'est trouvé
     */
    @SuppressWarnings("unchecked")
    public void parseField(String name, String str) throws NumberFormatException, NullPointerException {
        Field<?> field = fields.get(name);
        if (field.get() instanceof Boolean) {
            ((Field<Boolean>) field).set(Boolean.valueOf(str));
        } else if (field.get() instanceof Double) {
            ((Field<Double>) field).set(Double.valueOf(str));
        } else if (field.get() instanceof Integer) {
            ((Field<Integer>) field).set(Integer.valueOf(str));
        } else if (field.get() instanceof String) {
            ((Field<String>) field).set(str);
        }
    }

    /**
     * @return l'ensemble des champs
     */
    public Field<?>[] getFields() {
        return fields.values().toArray(new Field[0]);
    }

    /**
     * @return true si le cluster est verrouillé
     */
    public boolean isLocked() {
        return locked;
    }

    /**
     * Verrouille le cluster
     */
    public void lock() {
        locked = true;
    }

    /**
     * Déverouille le cluster
     */
    public void unlock() {
        locked = false;
    }

    /**
     * Change la valeur du verrou
     *
     * @param l booléen qui désign si le cluster sera verouillé ou pas
     */
    public void setLocked(boolean l) {
        locked = l;
    }

}
