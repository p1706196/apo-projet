package eugenisme.utils.field;

public class FieldLockedException extends RuntimeException {
    private static final long serialVersionUID = 1096320912220852894L;

    public FieldLockedException() {
        super();
    }

    public FieldLockedException(String msg) {
        super(msg);
    }
}