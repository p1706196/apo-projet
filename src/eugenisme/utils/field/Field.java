package eugenisme.utils.field;

/**
 * @param <T> le type associe au champ
 */
public class Field<T> {
    private String name;
    private T value;
    private FieldCluster fieldCluster;

    /**
     * Name doit etre unique dans chaque fieldcluster
     *
     * @param fieldCluster le fieldcluster contenant
     * @param name         le nom servant de clé pour la sauvegarde
     * @param value        la valeur du champ
     */
    // visibilite locale au pakage
    Field(FieldCluster fieldCluster, String name, T value) {
        this.name = name;
        this.value = value;
        this.fieldCluster = fieldCluster;
    }

    /**
     * @return la valeur du champ
     */
    public T get() {
        return value;
    }

    public Class<?> getType() {
        return value.getClass();
    }

    /**
     * @param val la nouvelle valeur du champ
     * @throws FieldLockedException si le champ est locked
     */
    public void set(T val) throws FieldLockedException {
        if (fieldCluster.isLocked()) throw new FieldLockedException();
        value = val;
    }

    /**
     * @return le nom
     */
    public String getName() {
        return name;
    }

    /**
     * rend le toString de la valeur
     */
    @Override
    public String toString() {
        return value.toString();
    }
}
