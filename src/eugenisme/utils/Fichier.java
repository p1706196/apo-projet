package eugenisme.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.TreeMap;

/**
 * Class Fichier, offre une interface pour lire un fichier de configuration
 */
public class Fichier {
    public class KeyNotFoundException extends Exception {
        private static final long serialVersionUID = -5357878789588662066L;

        public KeyNotFoundException() {
            super();
        }

        public KeyNotFoundException(String msg) {
            super(msg);
        }
    }

    private TreeMap<String, Object> data;

    public Fichier() {
        data = new TreeMap<String, Object>();
    }

    /**
     * Ajoute dans data les données lues dans le fichier
     * format :
     * <p>
     * key: boolean
     * key: "string"
     * key: 0
     * key : 0.0
     * <p>
     * (int puis double)
     *
     * @param path
     * @return true si la lecture a eu lieu
     */
    public boolean loadFromFile(String path) {
        BufferedReader br = null;
        try {
            br = new BufferedReader(new InputStreamReader(new FileInputStream(new File(path))));
            for (String line = br.readLine(); line != null; line = br.readLine()) {
                String[] couple = line.split(":");

                if (couple.length == 2) // si on n'est pas de la forme "key: val" on ignore
                {
                    // parse d'un objet
                    Object obj = parseItem(couple[1].trim());
                    if (obj != null) data.put(couple[0].trim(), obj);
                }
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                br.close();
            } catch (IOException e) {
            } catch (NullPointerException e) {
            }
        }
        return true;
    }

    /**
     * Parse le string donné en un type simple
     *
     * @param val la chaîne à parser
     * @return un Object instance de la meilleure représentation de la chaîne
     */
    private Object parseItem(String val) {
        if (val.equals("")) return null;
        Object obj = null;

        if (val.startsWith("\"") && val.endsWith("\"")) {
            obj = val.substring(1, val.length() - 1);
        } else {
            try {
                obj = Integer.valueOf(val);
            } catch (NumberFormatException e) {
                try {
                    obj = Double.valueOf(val);
                } catch (NumberFormatException ee) {
                    obj = Boolean.valueOf(val);
                }
            }

        }

        return obj;
    }

    /**
     * Sauvegarde les données dans un fichier texte
     *
     * @param path chemin du fichier
     * @return true si la sauvegarde a eu lieu
     * @throws IOException si erreur lors de l'écriture
     */
    public boolean saveToFile(String path) {
        FileWriter fw = null;

        try {
            File f = new File(path);
            f.createNewFile();
            fw = new FileWriter(f);
            fw.write(toString());
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                fw.close();
            } catch (IOException e1) {
            } catch (NullPointerException e2) {
            }
        }
        return true;
    }

    /// getters ///

    /**
     * getType renvoie un objet de type Type ou une erreur si la clé n'existe pas
     * getTypeOrDefault idem mais renvoie la valeur donnée si la clé n'existe pas
     */

    public boolean getBoolean(String key) throws KeyNotFoundException {
        Object obj = data.get(key);
        if (obj != null) {
            if (obj instanceof Boolean)
                return (Boolean) obj;
        }
        throw new KeyNotFoundException();
    }

    public boolean getBooleanOrDefault(String key, boolean def) {
        try {
            return getBoolean(key);
        } catch (KeyNotFoundException e) {
            return def;
        }
    }

    public int getInteger(String key) throws KeyNotFoundException {
        Object obj = data.get(key);
        if (obj != null) {
            if (obj instanceof Integer)
                return (Integer) obj;
        }
        throw new KeyNotFoundException();
    }

    public int getIntegerOrDefault(String key, int def) {
        try {
            return getInteger(key);
        } catch (KeyNotFoundException e) {
            return def;
        }
    }

    public double getDouble(String key) throws KeyNotFoundException {
        Object obj = data.get(key);
        if (obj != null) {
            if (obj instanceof Double)
                return (Double) obj;
        }
        throw new KeyNotFoundException();
    }

    public double getDoubleOrDefault(String key, double def) {
        try {
            return getDouble(key);
        } catch (KeyNotFoundException e) {
            return def;
        }
    }

    public String getString(String key) throws KeyNotFoundException {
        Object obj = data.get(key);
        if (obj != null) {
            if (obj instanceof String)
                return (String) obj;
        }
        throw new KeyNotFoundException();
    }

    public String getStringOrDefault(String key, String def) {
        try {
            return getString(key);
        } catch (KeyNotFoundException e) {
            return def;
        }
    }


    /// setters ///
    public void setBoolean(String key, boolean value) {
        data.put(key, new Boolean(value));

    }

    public void setInteger(String key, int value) {
        data.put(key, new Integer(value));

    }

    public void setDouble(String key, double value) {
        data.put(key, new Double(value));

    }

    public void setString(String key, String value) {
        data.put(key, value);

    }


    @Override
    public String toString() {
        String ret = "";
        for (Map.Entry<String, Object> entr : data.entrySet()) {
            ret += entr.getKey() + ": ";
            ret += entr.getValue() instanceof String ? "\"" : "";
            ret += entr.getValue().toString();
            ret += entr.getValue() instanceof String ? "\"" : "";
            ret += "\n";
        }
        return ret;
    }
}
