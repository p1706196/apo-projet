package eugenisme.model;

/**
 * Exception générée lorsque l'on lance une expérience non initialisée
 */
public class NotInitializedException extends RuntimeException {
    private static final long serialVersionUID = -3111230864596490643L;

    public NotInitializedException() {
        super();
    }

    public NotInitializedException(String msg) {
        super(msg);
    }
}