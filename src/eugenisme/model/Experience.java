package eugenisme.model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import eugenisme.model.ant.Fourmi;
import eugenisme.model.ant.Gene;
import eugenisme.model.world.Monde;
import eugenisme.utils.field.Field;
import eugenisme.utils.field.FieldCluster;

/**
 * La class Experience possède tous les paramètres nécessaires à l'exécution du programme et gère son exécution
 */
public class Experience {
    // proprietes
    // on charge les differents champs
    public final FieldCluster fieldCluster = new FieldCluster();
    public final Field<Integer> nbFourmi = fieldCluster.getNewField("nbFourmi", 2);
    public final Field<Integer> tailleSelectionTopFourmi = fieldCluster.getNewField("tailleSelectionTopFourmi", 1);
    public final Field<Integer> nbGen = fieldCluster.getNewField("nbGen", 1);
    public final Field<Integer> tempsSim = fieldCluster.getNewField("tempsSim", 1);
    public final Field<Integer> mondeHeight = fieldCluster.getNewField("mondeHeight", 10);
    public final Field<Integer> mondeWidth = fieldCluster.getNewField("mondeWidth", 10);
    public final Field<Integer> nbSourceNourriture = fieldCluster.getNewField("nbSourceNourriture", 5);
    public final Field<Integer> nbNourritureDansSourceMin = fieldCluster.getNewField("nbNourritureDansSourceMin", 1);
    public final Field<Integer> nbNourritureDansSourceMax = fieldCluster.getNewField("nbNourritureDansSourceMax", 3);
    public final Field<Integer> nbFourmiliere = fieldCluster.getNewField("nbFourmiliere", 1);
    public final Field<Boolean> nouveauMondeChaqueGen = fieldCluster.getNewField("nouveauMondeChaqueGen", false);
    public final Field<Boolean> utiliserMondeSauvegarde = fieldCluster.getNewField("utiliserMondeSauvegarde", false);
    public final Field<Boolean> sauvegarderDernierMonde = fieldCluster.getNewField("sauvegarderDernierMonde", false);
    public final Field<Integer> nbFourmiASauvegarder = fieldCluster.getNewField("nbFourmiASauvegarder", 10);
    public final Field<String> fichierFourmi = fieldCluster.getNewField("fichierFourmi", ".fourmis");
    public final Field<String> fichierMonde = fieldCluster.getNewField("fichierMonde", ".monde");
    public final Field<Integer> nbThread = fieldCluster.getNewField("nbThread", 1);
    public final Field<Double> probaMutationAction = fieldCluster.getNewField("probaMutationAction", 0.2);
    public final Field<Double> probaMutationCondition = fieldCluster.getNewField("probaMutationCondition", 0.1);
    public final Field<Double> probaMutationEchange = fieldCluster.getNewField("probaMutationEchange", 0.1);
    public final Field<Boolean> afficherFenetreGenesFourmi = fieldCluster.getNewField("afficherFenetreGenesFourmi", true);
    public final Field<Boolean> afficherFenetreSimulation = fieldCluster.getNewField("afficherFenetreSimulation", true);

    public final static String TMP_FILE_SETTINGS = ".settings";
    public final static String TMP_FILE_MONDE = ".monde";
    public final static String TMP_FILE_FOURMIS = ".fourmis";
    public final static String TMP_FILE_PAUSED = ".paused";
    // variables de fonctionnement
    private final static Random RANDOM_GENERATOR = new Random();

    private int nbCompletedGen = 0;
    private Monde mondeOriginal;
    private volatile ArrayList<Fourmi> fourmis = new ArrayList<Fourmi>();
    private int scoreMin = 0;
    private int scoreMoy = 0;
    private int scoreMax = 0;
    private long elapsedTime = 0;

    /**
     * Le constructeur
     */
    public Experience() {

    }

    /**
     * Initialise l'experience et s'assure qu'elle est dans un état qui permet sa bonne exécution et gèle les modifications
     */
    public void initialize() throws InitializationFailException {
        if (fieldCluster.isLocked()) throw new InitializationFailException("Already initialized");
        // verification de l'etat du fichier de sauvegarde du monde
        if (utiliserMondeSauvegarde.get()) {
            File file = new File(fichierMonde.get());
            if (!(file.isFile() && file.canRead()))
                throw new InitializationFailException("Le chemin de Monde ne peut pas etre lut");
        }
        if (sauvegarderDernierMonde.get()) {
            File file = new File(fichierMonde.get());
            if (!file.isFile()) {
                try {
                    if (!file.createNewFile()) {
                        if (!(file.isFile() && file.canWrite()))
                            throw new InitializationFailException("Le chemin de Monde ne peut pas etre ecrit (Impossible de creer le fichier)");
                    }
                } catch (IOException e) {
                    throw new InitializationFailException("Le chemin de Monde ne peut pas etre ecrit (IOException)");
                }
            } else {
                if (!file.canWrite())
                    throw new InitializationFailException("Le chemin de Monde ne peut pas etre ecrit");
            }
        }

        // verification de l'etat du fichier de sauvegarde des fourmis
        if (nbFourmiASauvegarder.get() > 0) {
            File file = new File(fichierFourmi.get());
            if (!file.isFile()) {
                try {
                    if (!file.createNewFile()) {
                        if (!(file.isFile() && file.canWrite()))
                            throw new InitializationFailException("Le chemin de Fourmi ne peut pas etre ecrit (Impossible de creer le fichier)");
                    }
                } catch (IOException e) {
                    throw new InitializationFailException("Le chemin de Fourmi ne peut pas etre ecrit (IOException)");
                }
            } else {
                if (!file.canWrite())
                    throw new InitializationFailException("Le chemin de Fourmi ne peut pas etre ecrit");
            }
        }

        // verifier que les constantes sont valides

        if (tailleSelectionTopFourmi.get() > nbFourmi.get())
            throw new InitializationFailException("Le nombre de fourmis selectionnees a chaque generaton ne peut pas etre plus grand que le nombre total de fourmis");
        if (nbFourmiASauvegarder.get() > tailleSelectionTopFourmi.get())
            throw new InitializationFailException("Le nombre de fourmis a sauvegarder a la fin de l'experience ne peut pas etre plus grand que le nombre de fourmis gardees a la fin d'un generation");

        if (nbThread.get() < 1) //si le nombre de thread est negatif (ou 0) on prendre le nombre max de thread
        {
            nbThread.set(Runtime.getRuntime().availableProcessors());
        }

        //validite des probas
        if (0 > probaMutationAction.get() || probaMutationAction.get() > 1)
            throw new InitializationFailException("Une probabilite doit etre entre 0 et 1 (probaMutationAction=" + probaMutationAction + ")");
        if (0 > probaMutationCondition.get() || probaMutationCondition.get() > 1)
            throw new InitializationFailException("Une probabilite doit etre entre 0 et 1 (probaMutationCondition=" + probaMutationCondition + ")");
        if (0 > probaMutationEchange.get() || probaMutationEchange.get() > 1)
            throw new InitializationFailException("Une probabilite doit etre entre 0 et 1 (probaMutationEchange=" + probaMutationEchange + ")");

        // debut reel de l'initialisation

        nbCompletedGen = 0;

        genererMonde();

        fourmis = new ArrayList<Fourmi>();
        for (int i = 0; i < nbFourmi.get(); i++) {
            fourmis.add(new Fourmi(new Gene()));
        }

        fieldCluster.lock();
    }

    /**
     * Fonction à appeler après la fin de la simuation pour sauvegarder les données
     */
    public void save() {
        if (!fieldCluster.isLocked()) throw new NotInitializedException();
        // sauvegarder le monde
        if (sauvegarderDernierMonde.get())
            mondeOriginal.saveToFile(fichierMonde.get());
        // sauvegarder les fourmis
        if (nbFourmiASauvegarder.get() > 0) {
            String sav = "";
            for (int i = 0; i < nbFourmiASauvegarder.get(); i++) {
                sav += fourmis.get(i).toString() + "\n";
            }
            // ecriture de fourmi
            FileWriter fw = null;

            try {
                File f = new File(fichierFourmi.get());
                f.createNewFile();
                fw = new FileWriter(f);
                fw.write(sav);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    fw.close();
                } catch (IOException e1) {
                } catch (NullPointerException e2) {
                }
            }
        }
    }

    /**
     * Fonction de sauvegarde totale de l'état de l'expérience, fonctionne si on veut mettre en pause le programme
     */
    public void pauseToFile() {
        if (!fieldCluster.isLocked()) throw new NotInitializedException();
        // sauvegarde des config
        saveSettingsToFile(TMP_FILE_SETTINGS);

        // sauvegarder le monde sans condition
        mondeOriginal.saveToFile(TMP_FILE_MONDE);
        // sauvegarder toutes les fourmis
        String sav = "";
        for (int i = 0; i < nbFourmi.get(); i++) {
            sav += fourmis.get(i).toString() + "\n";
        }

        // ecriture de fourmi
        FileWriter fw = null;
        try {
            File f = new File(TMP_FILE_FOURMIS);
            f.createNewFile();
            fw = new FileWriter(f);
            fw.write(sav);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                fw.close();
            } catch (IOException e1) {
            } catch (NullPointerException e2) {
            }
        }

        // sauvegarde de nbCompletedGen et elasedtime
        fw = null;
        try {
            File f = new File(TMP_FILE_PAUSED);
            f.createNewFile();
            fw = new FileWriter(f);
            fw.write("" + nbCompletedGen + "\r\n" + elapsedTime);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                fw.close();
            } catch (IOException e1) {
            } catch (NullPointerException e2) {
            }
        }
    }

    /**
     * Fonction de chargement total depuis les fichiers, à utiliser en sortie de pause
     *
     * @throws InitializationFailException
     */
    public void resumeFromFile() throws InitializationFailException {
        if (fieldCluster.isLocked()) throw new InitializationFailException("Already initialized");
        //chargement des parametres precedents
        loadSettingsFromFile(TMP_FILE_SETTINGS);
        initialize();
        new File(TMP_FILE_SETTINGS).delete();

        //lecture monde
        mondeOriginal.loadFromFile(TMP_FILE_MONDE);
        new File(TMP_FILE_MONDE).delete();

        //lecture fourmis
        File ffile = new File(TMP_FILE_FOURMIS);
        BufferedReader br = null;
        try {
            br = new BufferedReader(new InputStreamReader(new FileInputStream(ffile)));
            for (String line = br.readLine(); line != null; line = br.readLine()) {
                fourmis.add(Fourmi.fromString(line));
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                br.close();
            } catch (IOException e) {
            } catch (NullPointerException e) {
            }
        }
        ffile.delete();

        //lecture nbCompletedGen et elasedtime
        br = null;
        File pfile = new File(TMP_FILE_PAUSED);
        try {
            br = new BufferedReader(new InputStreamReader(new FileInputStream(pfile)));
            nbCompletedGen = Integer.parseInt(br.readLine());
            elapsedTime = Long.parseLong(br.readLine());

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NumberFormatException e) {
            e.printStackTrace();
            nbCompletedGen = 0;
            elapsedTime = 0;
        } finally {
            try {
                br.close();
            } catch (IOException e) {
            } catch (NullPointerException e) {
            }
        }
        pfile.delete();
    }


    /**
     * Interrompt l'expérience, permet à nouveau l'édition des paramètres de simulation
     */
    public void invalidate() {
        fieldCluster.unlock();
    }

    /**
     * Fait une génération
     *
     * @return true si le nombre de générations maximales n'a pas été atteint, IE si l'expérience doit continuer
     */
    public boolean itererGeneration() {
        if (!fieldCluster.isLocked()) throw new NotInitializedException();
        long startTime = System.currentTimeMillis();

        if (nouveauMondeChaqueGen.get())
            genererMonde();

        // fait la simulation
        ArrayList<Thread> threads = new ArrayList<Thread>();
        int nbFourmisParThread = nbFourmi.get() / nbThread.get();

        for (int i = 0; i < nbThread.get() - 1; i++) {
            Thread thread = new Thread(new SimulationManager(mondeOriginal.clone(), tempsSim.get(), fourmis.subList(nbFourmisParThread * i, nbFourmisParThread * (i + 1)).toArray(new Fourmi[0])));
            thread.setDaemon(true);
            threads.add(thread);
            thread.start();
        }
        new SimulationManager(mondeOriginal.clone(), tempsSim.get(), fourmis.subList(nbFourmisParThread * (nbThread.get() - 1), nbFourmi.get()).toArray(new Fourmi[0])).run();

        // on attend la fin de tous les threads
        for (Thread t : threads) {
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }


        // fait l'evolution
        Collections.sort(fourmis); //tri

        // <3 stats
        scoreMax = fourmis.get(0).getScore();
        scoreMin = fourmis.get(fourmis.size() - 1).getScore();
        scoreMoy = 0;
        fourmis.forEach(f -> scoreMoy += f.getScore());
        scoreMoy /= fourmis.size();


        fourmis.subList(tailleSelectionTopFourmi.get(), nbFourmi.get()).clear(); // supprimes les moins bonnes fourmis
        while (fourmis.size() < nbFourmi.get()) // croisement des fourmis pour recrer la population
        {
            fourmis.add(new Fourmi(fourmis.get(RANDOM_GENERATOR.nextInt(tailleSelectionTopFourmi.get())).getGenome(),
                    fourmis.get(RANDOM_GENERATOR.nextInt(tailleSelectionTopFourmi.get())).getGenome(),
                    probaMutationAction.get(),
                    probaMutationCondition.get(),
                    probaMutationEchange.get()));
        }

        elapsedTime += (System.currentTimeMillis() - startTime);
        return ++nbCompletedGen < nbGen.get();
    }

    /**
     * Génère le monde selon les paramètres
     */
    private void genererMonde() {
        if (utiliserMondeSauvegarde.get()) {
            mondeOriginal = new Monde(mondeHeight.get(), mondeWidth.get());
            mondeOriginal.loadFromFile(fichierMonde.get());
        } else {
            mondeOriginal = new Monde(mondeHeight.get(), mondeWidth.get(), nbSourceNourriture.get(), nbNourritureDansSourceMin.get(), nbNourritureDansSourceMax.get(), nbFourmiliere.get());
        }
    }

    /**
     * Charge les paramètres depuis le fichier
     *
     * @param path le chemin du fichier
     */
    public void loadSettingsFromFile(String path) {
        fieldCluster.loadFromFile(path);
    }

    /**
     * Sauvegarde les paramètres dans le fichier
     *
     * @param path le chemin du fichier
     */
    public void saveSettingsToFile(String path) {
        fieldCluster.saveToFile(path);
    }

    ////////////////////////////////// Getters / Setters //////////////////////////////////

    /**
     * Les Setters sont désactivés quand l'expérience est initialisée
     */

    public Monde getMonde() {
        return mondeOriginal;
    }

    public ArrayList<Fourmi> getFourmis() {
        return fourmis;
    }

    public boolean HasReachedEnd() {
        return nbCompletedGen >= nbGen.get();
    }

    public int getNbCompletedGen() {
        return nbCompletedGen;
    }

    public boolean isInitialized() {
        return fieldCluster.isLocked();
    }

    public long getElapsedTime() {
        return elapsedTime;
    }

    public int getScoreMin() {
        return scoreMin;
    }

    public int getScoreMax() {
        return scoreMax;
    }

    public int getScoreMoy() {
        return scoreMoy;
    }
}
