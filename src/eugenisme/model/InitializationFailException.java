package eugenisme.model;

/**
 * Exception générée quand l'initialisation de l'expérience a échoué
 */
public class InitializationFailException extends Exception {
    private static final long serialVersionUID = -3131877701543252438L;

    public InitializationFailException() {
        super();
    }

    public InitializationFailException(String msg) {
        super(msg);
    }
}