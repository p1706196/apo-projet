package eugenisme.model;

import eugenisme.model.ant.Fourmi;
import eugenisme.model.world.Monde;

/**
 * Permet de rassembler les simulations d'une ou plusieurs fourmis pour pouvoir les exécuter entièrement dans des threads indépendants
 */
public class SimulationManager implements Runnable {
    private Fourmi[] fourmis;
    private Monde mondeOriginal;
    private int tempsSim;

    /**
     * @param monde    le monde original, dans lequel les foumis vivront, un clone de ce monde sera donné à chaque fourmi
     * @param dureeSim la durée de chaque simulation
     * @param fourmis  les fourmis simulées
     */
    public SimulationManager(Monde monde, int dureeSim, Fourmi... fourmis) {
        this.fourmis = fourmis;
        this.mondeOriginal = monde;
        this.tempsSim = dureeSim;
    }

    /**
     * Lance la simulation
     */
    @Override
    public void run() {
        for (Fourmi fourmi : fourmis) {
            // setup
            fourmi.initialize(mondeOriginal.clone());
            // execution de la simulation
            for (int i = 0; i < tempsSim; i++) {
                fourmi.update();
            }
            // application du score a la fourmi
            fourmi.calculerScore();
        }
    }

}
