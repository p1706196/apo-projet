/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eugenisme.model.world;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

/**
 * Monde représente la grille sur laquelle les fourmis vont évoluer
 */
public class Monde implements Cloneable {
    private static Random RANDOM_GENERATOR = new Random();

    private Case[][] plateau;

    /**
     * Construit un monde avec les dimensions données
     * le monde est entièrement constitué de cases vides au départ
     *
     * @param height la hauteur
     * @param width  la largeur
     */
    public Monde(int height, int width) {
        this.plateau = new Case[height][width];
        for (int i = 0; i < plateau.length; i++)
            for (int j = 0; j < plateau[0].length; j++) {
                plateau[i][j] = new Case();
            }
    }

    /**
     * Constructeur prenant en compte les paramètres de génération
     *
     * @param mondeHeight
     * @param mondeWidth
     * @param nbSourceNourriture
     * @param nbNourritureDansSourceMin
     * @param nbNourritureDansSourceMax
     * @param nbFourmiliere
     */
    public Monde(int mondeHeight, int mondeWidth, int nbSourceNourriture, int nbNourritureDansSourceMin, int nbNourritureDansSourceMax, int nbFourmiliere) {
        this(mondeHeight, mondeWidth);

        // generation sources de nouriture
        for (int i = 0; i < nbSourceNourriture; i++) {
            int h = RANDOM_GENERATOR.nextInt(mondeHeight);
            int w = RANDOM_GENERATOR.nextInt(mondeWidth);
            if (getCase(h, w).getClass() == Case.class)
                setCase(h, w, new SourceDeNourriture(nbNourritureDansSourceMin + RANDOM_GENERATOR.nextInt(nbNourritureDansSourceMax - nbNourritureDansSourceMin + 1)));
            else i--;
        }

        // generation fourmilieres
        for (int i = 0; i < nbFourmiliere; i++) {
            int h = RANDOM_GENERATOR.nextInt(mondeHeight);
            int w = RANDOM_GENERATOR.nextInt(mondeWidth);
            if (getCase(h, w).getClass() == Case.class)
                setCase(h, w, new Fourmiliere());
            else i--;
        }
    }


    /**
     * Parcourt le tableau è la recherche de source de nourriture vide pour les remplacer par des cases vides
     */
    public void enleverSourcesVides() {

        for (int i = 0; i < plateau.length; i++) {
            for (int j = 0; j < plateau[i].length; j++) {

                if (plateau[i][j].isSourceDeNourriture()) {
                    SourceDeNourriture source = (SourceDeNourriture) plateau[i][j];
                    if (source.getNourritureRestante() == 0) {
                        plateau[i][j] = new Case();
                    }
                }

            }
        }

    }

    /**
     * Renvoie la case à la position donnée en paramètre
     *
     * @param h "Coordonnée correspondant à la hauteur
     * @param w "Coordonnée correspondant à la largeur
     * @return La Case correspondant aux coordonnées en assurant le fonctionnement torique.
     */
    public Case getCase(int h, int w) {
        return this.plateau[(getHeight() + (h % getHeight())) % getHeight()]
                [(getWidth() + (w % getWidth())) % getWidth()];
    }

    /**
     * Set la Case en respectant le fonctionnement torique
     *
     * @param h     "Coordonnée correspondant à la hauteur
     * @param w     "Coordonnée correspondant à la largeur
     * @param _case "La case à set
     */
    public void setCase(int h, int w, Case _case) {
        this.plateau[(getHeight() + (h % getHeight())) % getHeight()]
                [(getWidth() + (w % getWidth())) % getWidth()] = _case;
    }

    /**
     * Parcourt le plateau et fait la somme des compteurs de nourriture des fourmilières
     *
     * @return La somme des compteurs de nourriture des fourmilières du plateau
     */
    public int getCompteursFourmilieres() {

        int compteur = 0;

        for (int i = 0; i < plateau.length; i++) {
            for (int j = 0; j < plateau[i].length; j++) {

                if (plateau[i][j].isFourmiliere()) {
                    Fourmiliere fourmiliere = (Fourmiliere) plateau[i][j];
                    compteur += fourmiliere.getCompteurNourriture();
                }

            }
        }

        return compteur;

    }

    /**
     * Retourne la fourmilière la plus proche des coordonnées d'origine
     *
     * @param height la position d'origine en hauteur
     * @param width  la position d'origine en largeur
     * @return la Fourmiliere la plus proche
     */
    public CaseWrapper getNearestFourmiliereFrom(int height, int width) {
        CaseWrapper near = new CaseWrapper(null, 0, 0);
        int dist = 0;
        for (int h = 0; h < getHeight(); h++) {
            for (int w = 0; w < getWidth(); w++) {
                if (plateau[h][w] instanceof Fourmiliere) {
                    if (near._case == null || (getDistance(height, width, h, w) < dist)) {
                        near._case = plateau[h][w];
                        dist = getDistance(height, width, h, w);
                        near.posH = h;
                        near.posW = w;

                    }
                }
            }
        }
        return near;
    }

    /**
     * Retourne la fourmilière la plus proche des coordonnées d'origine
     *
     * @param height la position d'origine en hauteur
     * @param width  la position d'origine en largeur
     * @return la Fourmiliere la plus proche
     */
    public CaseWrapper getNearestSourceDeNourritureFrom(int height, int width) {
        CaseWrapper near = new CaseWrapper(null, 0, 0);
        int dist = 0;
        for (int h = 0; h < getHeight(); h++) {
            for (int w = 0; w < getWidth(); w++) {
                if (plateau[h][w] instanceof SourceDeNourriture) {
                    if (near._case == null || (getDistance(height, width, h, w) < dist)) {
                        near._case = plateau[h][w];
                        dist = getDistance(height, width, h, w);
                        near.posH = h;
                        near.posW = w;

                    }
                }
            }
        }
        return near;
    }


    /**
     * Donne la distance entre deux points du monde torique
     *
     * @param posH1
     * @param posW1
     * @param posH2
     * @param posW2
     * @return la distance entre (posH1, posW1) et (posH2,posW2)
     */
    public int getDistance(int posH1, int posW1, int posH2, int posW2) {
        int h1 = (getHeight() + (posH1 % getHeight())) % getHeight();
        int w1 = (getWidth() + (posW1 % getWidth())) % getWidth();
        int h2 = (getHeight() + (posH2 % getHeight())) % getHeight();
        int w2 = (getWidth() + (posW2 % getWidth())) % getWidth();
        int distH = Math.abs(h1 - h2);
        int distW = Math.abs(w1 - w2);
        return
                (distH > getHeight() / 2 ? getHeight() - distH : distH) +
                        (distW > getWidth() / 2 ? getWidth() - distW : distW);
    }

    /**
     * Sauvergarde le monde dans un fichier
     *
     * @param fileName le chemin vers le fichier à écrire
     * @return true si la sauvegarde à eu lieu
     */
    public boolean saveToFile(String fileName) {
        FileWriter fw = null;

        try {
            File f = new File(fileName);
            f.createNewFile();
            fw = new FileWriter(f);
            fw.write(toString());
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                fw.close();
            } catch (IOException e1) {
            } catch (NullPointerException e2) {
            }
        }
        return true;
    }

    /**
     * Charge le monde depuis un fichier, par du principe que le monde est déjà de la bonne taille
     *
     * @param fileName le chemin vers le fichier
     * @return true si le chargement s'est correctement passé
     */
    public boolean loadFromFile(String fileName) {
        BufferedReader br = null;
        try {
            br = new BufferedReader(new InputStreamReader(new FileInputStream(new File(fileName))));
            int i = 0;
            // on prend chaque ligne
            for (String line = br.readLine(); line != null && i < getHeight(); line = br.readLine()) {
                String[] row = line.trim().split(" ");
                int j = 0;
                // et on la lit en largueur
                for (int p = 0; p < row.length && j < getWidth(); p++) {
                    String val = row[p];
                    if (val.startsWith("c")) {
                        plateau[i][j] = new Case();
                        j++;
                    } else if (val.startsWith("f")) {
                        plateau[i][j] = new Fourmiliere(Integer.parseInt(val.substring(1)));
                        j++;
                    } else if (val.startsWith("n")) {
                        plateau[i][j] = new SourceDeNourriture(Integer.parseInt(val.substring(1)));
                        j++;
                    }
                    // si aucun patern ne correspond, on n'incremente pas j, permet d'ignorer les espaces supplementaires eventuels

                }
                i++;
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
            return false;
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                br.close();
            } catch (IOException e) {
            } catch (NullPointerException e) {
            }
        }
        return true;
    }

    public int getHeight() {
        return plateau.length;
    }

    public int getWidth() {
        return plateau[0].length;
    }

    public int getDistanceMax() {
        return (getHeight() + getWidth()) / 2;
    }

    @Override
    public Monde clone() {
        int height = getHeight();
        int width = getWidth();

        Monde m = new Monde(height, width);
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                m.plateau[i][j] = this.plateau[i][j].clone();
            }
        }
        return m;
    }

    @Override
    public String toString() {
        String ret = "";
        for (int j = 0; j < getHeight(); j++) {
            for (int i = 0; i < getWidth(); i++) {
                ret += plateau[j][i].toString() + " ";
            }
            ret += "\n";
        }
        return ret;
    }

}
