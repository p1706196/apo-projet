/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eugenisme.model.world;

/**
 *
 */
public class Fourmiliere extends Case implements Cloneable {

    private int compteurNourriture;

    public Fourmiliere() {
        super();
        this.compteurNourriture = 0;
    }

    public Fourmiliere(int compteurNourriture) {
        this();
        this.compteurNourriture = compteurNourriture;
    }

    @Override
    public boolean isFourmiliere() {
        return true;
    }

    @Override
    public boolean isSourceDeNourriture() {
        return false;
    }

    public int getCompteurNourriture() {
        return this.compteurNourriture;
    }

    public void deposerNourriture() {
        this.compteurNourriture++;

    }

    @Override
    public Fourmiliere clone() {
        return new Fourmiliere(compteurNourriture);
    }

    @Override
    public String toString() {
        return "f" + compteurNourriture;
    }
}

