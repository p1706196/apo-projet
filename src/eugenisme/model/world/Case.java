/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eugenisme.model.world;

/**
 *
 */
public class Case implements Cloneable {
    public Case() {

    }

    public boolean isFourmiliere() {
        return false;
    }

    public boolean isSourceDeNourriture() {
        return false;
    }

    @Override
    public Case clone() {
        return new Case();
    }

    @Override
    public String toString() {
        return "c";
    }
}
