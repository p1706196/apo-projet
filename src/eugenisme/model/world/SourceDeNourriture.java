/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eugenisme.model.world;

/**
 *
 */
public class SourceDeNourriture extends Case implements Cloneable {

    private int nourritureRestante;

    public SourceDeNourriture(int nourritureRestante) {
        super();
        this.nourritureRestante = nourritureRestante;
    }

    @Override
    public boolean isFourmiliere() {
        return false;
    }

    @Override
    public boolean isSourceDeNourriture() {
        return true;
    }

    public int getNourritureRestante() {
        return this.nourritureRestante;
    }

    /**
     * @return true si la source est vide est doit être supprimée
     */
    public boolean retirerNourriture() {
        this.nourritureRestante--;
        return nourritureRestante <= 0;
    }

    public void ajouterNourriture() {
        this.nourritureRestante++;

    }

    @Override
    public SourceDeNourriture clone() {
        return new SourceDeNourriture(nourritureRestante);
    }

    @Override
    public String toString() {
        return "n" + nourritureRestante;
    }
}
