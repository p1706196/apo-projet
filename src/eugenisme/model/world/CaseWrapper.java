package eugenisme.model.world;

/**
 * la class CaseWrapper permet d'avoir un objet contenant une case et sa position
 */
public class CaseWrapper {
    public Case _case;
    public int posH;
    public int posW;

    public CaseWrapper(Case _case, int posH, int posW) {
        this._case = _case;
        this.posH = posH;
        this.posW = posH;
    }

    @Override
    public String toString() {
        return _case.toString() + "(" + posH + "," + posW + ")";
    }
}