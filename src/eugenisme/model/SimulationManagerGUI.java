package eugenisme.model;

import eugenisme.model.ant.Fourmi;
import eugenisme.model.world.Monde;
import eugenisme.view.GUI;

/**
 * Permet de rassembler les simulations d'une ou plusieurs fourmis pour pouvoir les exécuter entièrement dans des threads indépendants
 * et d'afficher leur progression dans une fenêtre
 */
public class SimulationManagerGUI implements Runnable {
    private Fourmi fourmi;
    private Monde mondeOriginal;
    private int tempsSim;
    private GUI gui;

    /**
     * @param monde    le monde original, dans lequel les fourmis vivront, un clone de ce monde sera donné à chaque fourmi
     * @param dureeSim la durée de chaque simulation
     * @param fourmi   les fourmis simulées
     */
    public SimulationManagerGUI(Monde monde, int dureeSim, Fourmi fourmi) {
        this.fourmi = fourmi;
        this.mondeOriginal = monde;
        this.tempsSim = dureeSim;
        this.gui = new GUI(monde, fourmi);
    }

    /**
     * Lance la simulation
     */
    @Override
    public void run() {

        gui.startUI();
        // setup
        fourmi.initialize(mondeOriginal.clone());
        // execution de la simulation
        for (int i = 0; i < tempsSim; i++) {
            fourmi.update();
            gui.updateUI();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                i = tempsSim;
            }
        }
        // application du score a la fourmi
        fourmi.calculerScore();

    }

}
