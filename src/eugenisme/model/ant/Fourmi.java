/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eugenisme.model.ant;

import java.util.Random;

import eugenisme.model.world.Case;
import eugenisme.model.world.CaseWrapper;
import eugenisme.model.world.Fourmiliere;
import eugenisme.model.world.Monde;
import eugenisme.model.world.SourceDeNourriture;

/**
 * Fourmi qui évolue dans un monde en suivant un arbre de comportement
 */
public class Fourmi implements Comparable<Fourmi> {

    private static volatile Random RANDOM_GENERATOR = new Random();

    /**
     * Enumeration des différentes directions
     */
    public enum Direction {
        UP,
        DOWN,
        RIGHT,
        LEFT;

        /**
         * @return une direction au hasard
         */
        public static Direction rand() {
            return Direction.values()[RANDOM_GENERATOR.nextInt(Direction.values().length)];
        }
    }

    /**
     * position en hauteur et en largeur
     */
    private int posH = 0;
    private int posW = 0;
    private int score = 0;
    private boolean porteNourriture = false;
    private Gene genome;

    private Monde monde = null;

    public Fourmi(Gene genome) {
        this.genome = genome;
    }

    /**
     * Créer une nouvelle fourmi avec un arbre de Gene résultant du mélange de l'arbre d'un parent
     * avec le sous arbre de l'autre parent
     * La fonction garantie que les gènes donnés en paramètre ne sont pas modifiés
     *
     * @param genomeMere Gene da la fourmi mère (un des deux parents)
     * @param genomePere Gene de la fourmi père (un des deux parents)
     */
    public Fourmi(Gene genomeMere, Gene genomePere, double probaMutationAction, double probaMutationCondition, double probaMutationEchange) {
        this(genomeMere.clone());
        genome.breed(genomePere);
        genome.mutation(probaMutationAction, probaMutationCondition, probaMutationEchange);
        genome.clean();
    }

    private Fourmi() {
    }

    /**
     * retourne une Fourmi à partir d'une chaine
     * exemple
     * <p>
     * Fourmi f = Fourmi.fromString("posH: 9; posW: 0; nourriture: true; score: 20; genome: PORTE_NOURRITURE[RENTRER_FOURMILIERE, DEPLACEMENT_HAUT]");
     * <p>
     * en réalité seul le paramètre génome est indispensable
     *
     * @param representation la chaîne à parse
     * @return la Fourmi construite
     * @throws IllegalArgumentException si il n'y a pas de génome
     */
    public static Fourmi fromString(String representation) {
        Fourmi ret = new Fourmi();
        String[] args = representation.trim().split(";");

        for (String elem : args) {
            String[] param = elem.trim().split(":");
            switch (param[0].trim()) {
                case "posH":
                    ret.posH = Integer.parseInt(param[1].trim());
                    break;
                case "posW":
                    ret.posW = Integer.parseInt(param[1].trim());
                    break;
                case "nourriture":
                    ret.porteNourriture = Boolean.parseBoolean(param[1].trim());
                    break;
                case "score":
                    ret.score = Integer.parseInt(param[1].trim());
                    break;
                case "genome":
                    ret.genome = Gene.fromString(param[1].trim());
                    break;
                default:
                    break;
            }
        }

        if (ret.genome == null) throw new IllegalArgumentException("La chaine donnee ne contient pas de genome");

        return ret;
    }


    /**
     * Cette méthode va lancer le comportement de la fourmi pour lui faire faire une action
     * pour une iteration de la simulation
     */
    public void update() {
        boolean choix = false;
        // parcours du gene jusqu'a atteindre une feuille
        for (Gene g = genome; g != null; g = g.getNext(choix)) {
            // on fait l'action donnee par le type du gene.
            choix = g.getType().exec(this);
        }
    }

    /**
     * remet la Fourmi dans l'état de depart avant la simulation
     */
    public void initialize(Monde monde) {
        this.posH = monde.getHeight() / 2;
        this.posW = monde.getWidth() / 2;
        this.score = 0;
        this.porteNourriture = false;
        this.monde = monde;
    }

    /**
     * calcule le score et le stocke dans score
     */
    public void calculerScore() {
        // distance a la fourmiliere la plus proche
        CaseWrapper cwF = monde.getNearestFourmiliereFrom(getPosH(), getPosW());
        int distF = monde.getDistance(getPosH(), getPosW(), cwF.posH, cwF.posW);

        // distance a la source de nouriture la plus proche
        CaseWrapper cwN = monde.getNearestSourceDeNourritureFrom(getPosH(), getPosW());
        int distN = monde.getDistance(getPosH(), getPosW(), cwN.posH, cwN.posW);

        score = (monde.getCompteursFourmilieres() * 3 + (doPorteNourriture() ? 1 : 0)) * 2 * monde.getDistanceMax() - (doPorteNourriture() ? distF : distN);
    }


    /**
     * @return Un booléen qui informe si l'on est sur une source de nourriture ou non
     */
    public boolean detecterNourriture() {
        return monde.getCase(posH, posW).isSourceDeNourriture();
    }
    
    /**
     * L'observation se fait sur une distance correspondant à la taille du monde /2 car le monde est torique
     * 
     * @param direction la direction a observer
     * @return true si il y a une source de nourriture dans la direction donnée 
     */
    public boolean voirNourriture(Direction direction) {
        switch (direction) {
            case UP: //haut
            	for(int i = 1; i < monde.getHeight()/2; i++)
	            	if(monde.getCase(posH-i, posW).isSourceDeNourriture()) 
	            		return true;
            	return false;
            case DOWN: //bas
            	for(int i = 1; i < monde.getHeight()/2; i++)
	            	if(monde.getCase(posH+i, posW).isSourceDeNourriture()) 
	            		return true;
            	return false;
            case LEFT: //gauche
            	for(int i = 1; i < monde.getWidth()/2; i++)
	            	if(monde.getCase(posH, posW-i).isSourceDeNourriture()) 
	            		return true;
            	return false;
            case RIGHT: //droite
            	for(int i = 1; i < monde.getWidth()/2; i++)
	            	if(monde.getCase(posH, posW+i).isSourceDeNourriture()) 
	            		return true;
            	return false;
        }
        return false;
    }
    
    /**
     * L'observation se fait sur une distance correspondant à la taille du monde /2 car le monde est torique
     * 
     * @param direction la direction a observer
     * @return true si il y a une fourmilière dans la direction donnée 
     */
    public boolean voirFourmiliere(Direction direction) {
        switch (direction) {
            case UP: //haut
            	for(int i = 1; i < monde.getHeight()/2; i++)
	            	if(monde.getCase(posH-i, posW).isFourmiliere()) 
	            		return true;
            	return false;
            case DOWN: //bas
            	for(int i = 1; i < monde.getHeight()/2; i++)
	            	if(monde.getCase(posH+i, posW).isFourmiliere()) 
	            		return true;
            	return false;
            case LEFT: //gauche
            	for(int i = 1; i < monde.getWidth()/2; i++)
	            	if(monde.getCase(posH, posW-i).isFourmiliere()) 
	            		return true;
            	return false;
            case RIGHT: //droite
            	for(int i = 1; i < monde.getWidth()/2; i++)
	            	if(monde.getCase(posH, posW+i).isFourmiliere()) 
	            		return true;
            	return false;
        }
        return false;
    }


    /**
     * @return Un booléen qui informe si la fourmi a reussi à ramasser de la nourriture sur la case actuelle
     */
    public boolean ramasserNourriture() {

        //si la fourmi porte deja� de la nourriture, elle ne peut pas en porter plus
        if (porteNourriture) {
            return false;
        }

        //si on est sur une source, alors on prend de la nourriture
        if (monde.getCase(posH, posW).isSourceDeNourriture()) {
            SourceDeNourriture source = (SourceDeNourriture) monde.getCase(posH, posW);
            if (source.retirerNourriture()) {
                // si true alors on doit virer la source de nourriture
                monde.setCase(posH, posW, new Case());
            }
            this.porteNourriture = true;
            return true;
        } else {
            return false;
        }

    }

    /**
     * @return Un booléen qui informe si la fourmi a reussi à ramasser de la nourriture sur la case actuelle
     */
    public boolean deposerNourriture() {

        //si la fourmi porte deja� de la nourriture, elle ne peut pas en porter plus
        if (!porteNourriture)
            return false;

        //si on est sur une source, alors on prend de la nourriture
        if (monde.getCase(posH, posW).isSourceDeNourriture()) {
            SourceDeNourriture source = (SourceDeNourriture) monde.getCase(posH, posW);
            source.ajouterNourriture();
        } else if (monde.getCase(posH, posW).isFourmiliere()) {
            Fourmiliere source = (Fourmiliere) monde.getCase(posH, posW);
            source.deposerNourriture();
        } else {
            monde.setCase(posH, posW, new SourceDeNourriture(1));
        }

        porteNourriture = false;
        return true;
    }


    /**
     * @return Un booléen qui informe si l'on est sur une fourmilière ou non
     */
    public boolean detecterFourmiliere() {
        return monde.getCase(posH, posW).isFourmiliere();
    }

    /**
     * Déplace la fourmi sur le plateau. Cette methode respecte le monde "torique"
     * Le coin en haut a gauche a pour coordonnées 0,0
     *
     * @param direction La direction que la fourmi doit prendre
     */
    public void deplacement(Direction direction) {
        switch (direction) {

            case UP: //haut
                posH = (monde.getHeight() + posH - 1) % monde.getHeight();
                break;
            case DOWN: //bas
                posH = (posH + 1) % monde.getHeight();
                break;
            case LEFT: //gauche
                posW = (posW + (monde.getWidth() - 1)) % monde.getWidth();
                break;
            case RIGHT: //droite
                posW = (posW + 1) % monde.getWidth();
                break;
        }
    }


    /**
     * Déplace la fourmi d'une case du plateau vers la fourmilière la plus proche
     */
    public void deplacementVersFourmiliere() {
        CaseWrapper cw = monde.getNearestFourmiliereFrom(posH, posW);
        int dist = monde.getDistance(posH, posW, cw.posH, cw.posW);
        int h = posH;
        int w = posW;
        for (Direction d : Direction.values()) {
            deplacement(d);
            if (dist > monde.getDistance(posH, posW, cw.posH, cw.posW)) return;

            posH = h;
            posW = w;
        }
    }

    /**
     * Compare les fourmis en fonction de leur score
     */
    @Override
    public int compareTo(Fourmi autre) {
        return -(this.score - autre.score);
    }

    public boolean equals(Fourmi autre) {
        return this.compareTo(autre) == 0;
    }

    @Override
    public String toString() {
        String ret = "";
        ret += "posH: " + posH;
        ret += "; posW: " + posW;
        ret += "; nourriture: " + porteNourriture;
        ret += "; score: " + score;
        ret += "; genome: " + genome;

        return ret;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public Gene getGenome() {
        return genome;
    }

    public boolean doPorteNourriture() {
        return porteNourriture;
    }

    public int getPosH() {
        return posH;
    }

    public int getPosW() {
        return posW;
    }

    public int getScore() {
        return score;
    }

    /**
     * monde est null tant que la Fourmi n'a pas été initialisée
     *
     * @return le monde courant dans lequel la foumi est
     */
    public Monde getMonde() {
        return monde;
    }
}
