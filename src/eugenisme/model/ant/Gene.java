package eugenisme.model.ant;

import java.io.IOException;
import java.io.StringReader;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

import eugenisme.model.ant.Fourmi.Direction;

/**
 * Noeud de l'arbre de comportement d'une fourmi
 */
public class Gene implements Cloneable {
    /**
     * Générateur aléatoire
     */
    private static Random RANDOM_GENERATOR = new Random();

    /**
     * Sert de lambda pour définir le comportement des gènes
     */
    public interface Function {
        public boolean exec(Fourmi fourmi);
    }

    /**
     * Enumeration des différents types de gènes possibles
     * les types de noeuds (sous NODE_COUNT) puis les types de feuilles
     */
    public enum GeneType {
        /// Capteurs
        DETECTE_NOURRITURE(fourmi -> fourmi.detecterNourriture()),
        DETECTE_FOURMILIERE(fourmi -> fourmi.detecterFourmiliere()),
        PORTE_NOURRITURE(fourmi -> fourmi.doPorteNourriture()),
        RANDOM_CHOICE(false, fourmi -> RANDOM_GENERATOR.nextBoolean()),
        
        VOIT_NOURRITURE_HAUT(fourmi -> fourmi.voirNourriture(Direction.UP)),
        VOIT_NOURRITURE_BAS(fourmi -> fourmi.voirNourriture(Direction.DOWN)),
        VOIT_NOURRITURE_GAUCHE(fourmi -> fourmi.voirNourriture(Direction.LEFT)),
        VOIT_NOURRITURE_DROITE(fourmi -> fourmi.voirNourriture(Direction.RIGHT)),
        
        VOIT_FOURMILIERE_HAUT(fourmi -> fourmi.voirFourmiliere(Direction.UP)),
        VOIT_FOURMILIERE_BAS(fourmi -> fourmi.voirFourmiliere(Direction.DOWN)),
        VOIT_FOURMILIERE_GAUCHE(fourmi -> fourmi.voirFourmiliere(Direction.LEFT)),
        VOIT_FOURMILIERE_DROITE(fourmi -> fourmi.voirFourmiliere(Direction.RIGHT)),

        NODE_COUNT(null),

        /// Actions
        DEPLACEMENT_HAUT(fourmi -> {
            fourmi.deplacement(Direction.UP);
            return true;
        }),
        DEPLACEMENT_BAS(fourmi -> {
            fourmi.deplacement(Direction.DOWN);
            return true;
        }),
        DEPLACEMENT_GAUCHE(fourmi -> {
            fourmi.deplacement(Direction.LEFT);
            return true;
        }),
        DEPLACEMENT_DROITE(fourmi -> {
            fourmi.deplacement(Direction.RIGHT);
            return true;
        }),
        //DEPLACEMENT_RAND(fourmi -> {fourmi.deplacement(Direction.rand()); return true;}),
        RAMASSER_NOURRITURE(fourmi -> fourmi.ramasserNourriture()),
        DEPOSER_NOURRITURE(fourmi -> fourmi.deposerNourriture()),
        //RENTRER_FOURMILIERE(fourmi -> { fourmi.deplacementVersFourmiliere(); return true; }),

        LEAF_COUNT_AND_NODE_COUNT_AND_ONE(null);

        private Function fun;
        private boolean b_canBeSimplified = true;

        /**
         * @param func la fonction associée au GeneType
         */
        private GeneType(Function func) {
            fun = func;
        }

        /**
         * @param func la fonction associée au GeneType
         */
        private GeneType(boolean _canBeSimplified, Function func) {
            this(func);
            b_canBeSimplified = _canBeSimplified;
        }

        /**
         * @param f la fourmi sur laquelle le Gene intervient
         * @return true ou false en fonction du sous-gène suivant : si le gène est un noeud, ou si c'est une feuille
         */
        public boolean exec(Fourmi f) {
            return fun.exec(f);
        }

        /**
         * @return si le noeud en question peut être simplifié
         */
        public boolean canBeSimplified() {
            return b_canBeSimplified;
        }
    }

    private GeneType type;
    private Gene geneVrai = null;
    private Gene geneFaux = null;
    private boolean b_isRoot = false;

    /**
     * construit un gène aléatoire
     * Ce Gene est automatiquement la racine
     */
    public Gene() {
        this(true);
    }

    /**
     * Construit aléatoirement un Gene, si le Gene est racine il DOIT être un noeud et ses sous-noeuds ne sont pas des racines
     *
     * @param isRoot definit si le gène est racine
     */
    private Gene(boolean isRoot) {
        this.b_isRoot = isRoot;
        if (isRoot || RANDOM_GENERATOR.nextFloat() < 0.3) {
            type = GeneType.values()[RANDOM_GENERATOR.nextInt(GeneType.NODE_COUNT.ordinal())];
            geneVrai = new Gene(false);
            geneFaux = new Gene(false);
            clean();
        } else {
            type = GeneType.values()[RANDOM_GENERATOR.nextInt(GeneType.LEAF_COUNT_AND_NODE_COUNT_AND_ONE.ordinal() - GeneType.NODE_COUNT.ordinal() - 1) + GeneType.NODE_COUNT.ordinal() + 1];
        }
    }

    /**
     * construit un Gene feuille
     *
     * @param type le type de feuille
     * @throws InvalidParameterException si le type donné ne correspond pas à une feuille
     */
    public Gene(GeneType type) {
        this.b_isRoot = false;
        this.type = type;
        if (isNode())
            throw new InvalidParameterException("le type donne correspond a un noeud mais aucun sous noeud n'est specifie (type de feuille attendu)");
    }

    /**
     * construit un Gene noeud
     *
     * @param type     le type de noeud du gene
     * @param geneVrai son sous-gene vrai
     * @param geneFaux son sous-gene faux
     * @throws InvalidParameterException si le type donne ne correspond pas à un noeud
     */
    public Gene(GeneType type, Gene geneVrai, Gene geneFaux) {
        this.type = type;
        if (!isNode())
            throw new InvalidParameterException("le type ne correspond pas a un noeud mais des sous noeuds sont specifies (type de noeud attendu)");
        this.b_isRoot = true;
        this.geneVrai = geneVrai;
        this.geneVrai.b_isRoot = false;
        this.geneFaux = geneFaux;
        this.geneFaux.b_isRoot = false;
        clean();
    }

    /**
     * parse la chaîne et génère un Gene
     * exemple d'utilisation et syntaxe :
     * <p>
     * Gene g = Gene.fromString("DETECTE_NOURRITURE[DEPLACEMENT_GAUCHE, random]");
     * <p>
     * mots-clés :
     * random : gère un gène aléatoire par l'utilisation de new Gene()
     * null : représente un Gene null, ne devrait pas être utilisé
     *
     * @param str la chaîne a parse
     * @return le Gene représenté par la chaîne
     */
    public static Gene fromString(String str) {
        return fromStringReader(new StringReader(str));
    }

    /**
     * parse un Gene depuis un StringReader, permet de gérer le parse en interne
     *
     * @param stream le StringReader
     * @return le Gene représente par le stream
     */
    private static Gene fromStringReader(StringReader stream) {
        StringBuffer nameBuffer = new StringBuffer();
        //lecture du type
        try {
            int c = stream.read();
            while (nameBuffer.length() == 0 && c != -1) {
                while (c != '[' && c != ',' && c != ']' && c != -1) {
                    nameBuffer.append((char) c);
                    c = stream.read();
                }
                if (nameBuffer.length() == 0 && c != -1) c = stream.read();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        // parse du type
        String name = nameBuffer.toString().trim();

        if (name.equals("null")) return null;
        else if (name.equals("random")) return new Gene(false);
        GeneType _type = null;
        try {
            _type = GeneType.valueOf(name);
        } catch (IllegalArgumentException e) {
            System.err.println("Invalid GeneType : (" + name + ")");
            throw e;
        }
        // si on a lu un noeud
        if (_type.ordinal() < GeneType.NODE_COUNT.ordinal())
            return new Gene(_type, fromStringReader(stream), fromStringReader(stream));
        return new Gene(_type);
    }


    /**
     * nettoie l'arbre, appelle sa contrepartie privée récursive
     */
    public void clean() {
        int[] condition_init = new int[GeneType.NODE_COUNT.ordinal()];
        Arrays.fill(condition_init, -1);
        clean(condition_init);
    }

    /**
     * clean en sachant certaines propriétés
     * <p>
     * Le respect de la propriété de ROOT est en partie assurée par le fait que la racine ne peut avoir que -1 pour tous ses paramètres donc ne sera jamais supprimée par déterminisme
     *
     * @param condition_init tableau de conditions initiales : -1 si non determine, 0 si faux, 1 si vrai

     */
    private void clean(int[] condition_init) {
        // appel recursif
        if (isNode()) {
            int[] condition_vrai = condition_init.clone();
            int[] condition_faux = condition_init.clone();

            if (type.canBeSimplified()) {
                condition_vrai[type.ordinal()] = 1;
                condition_faux[type.ordinal()] = 0;
            }

            geneVrai.clean(condition_vrai);
            geneFaux.clean(condition_faux);

            // on peut supprimer le noeud si la condition qu'il represente est deja determine
            if (condition_init[type.ordinal()] == 0) copy(geneFaux);
            else if (condition_init[type.ordinal()] == 1) copy(geneVrai);
        }


        // si les sous genes sont identiques on peut supprimer le noeud
        if (isNode() && geneVrai.equals(geneFaux)) {
            // /!\ si le noeud actuel est root il ne faut pas le supprimer pour conserver la propriete de ROOT
            // dans ce cas on va forcer la mutation d'un sous gene
            if (b_isRoot) {
                boolean fOrV = RANDOM_GENERATOR.nextBoolean();
                (fOrV ? geneVrai : geneFaux).forceMutation();
            } else {
                copy(geneVrai);
            }
        }
    }

    /**
     * le gène actuel prend les valeurs du gène en paramètre
     * ce dernier n'est pas modifié
     * le gène actuel garde son statut de Root on non
     *
     * @param g
     */
    public void copy(Gene g) {
        this.type = g.type;
        if (isNode()) {
            this.geneFaux = g.geneFaux.clone();
            this.geneVrai = g.geneVrai.clone();
        } else {
            this.geneFaux = null;
            this.geneVrai = null;
        }
    }

    /**
     * Croise le gène actuel avec un gène du père choisit au hasard
     *
     * @param father                 le gène avec lequel croiser
     */
    public void breed(Gene father) {

        int idSousGenMere = b_isRoot ? (RANDOM_GENERATOR.nextInt(getNbSousGene() - 1) + 1) : RANDOM_GENERATOR.nextInt(getNbSousGene());
        int idSousGenPere = RANDOM_GENERATOR.nextInt(father.getNbSousGene());
        // fusion
        this.getSousGeneById(idSousGenMere).copy(father.getSousGeneById(idSousGenPere));
    }

    /**
     * Fait muter le gène, et ses fils, avec un certaine probabilité
     *
     * @param probaMutationAction    la probabilité qu'un gène d'Action mute
     * @param probaMutationCondition la probabilité qu'un gène de Condition mute
     * @param probaMutationEchange   la probabilité que les enfants soient echangés
     */
    public void mutation(double probaMutationAction, double probaMutationCondition, double probaMutationEchange) {
        if (RANDOM_GENERATOR.nextDouble() < (isNode() ? probaMutationCondition : probaMutationAction)) {
            int currentTypeCount = isNode() ? GeneType.NODE_COUNT.ordinal() : (GeneType.LEAF_COUNT_AND_NODE_COUNT_AND_ONE.ordinal() - GeneType.NODE_COUNT.ordinal() - 1);

            type = GeneType.values()[RANDOM_GENERATOR.nextInt(currentTypeCount) + (isNode() ? 0 : GeneType.NODE_COUNT.ordinal() + 1)];
        }
        if (isNode() && RANDOM_GENERATOR.nextDouble() < probaMutationEchange) {
            Gene tmp = geneVrai;
            geneVrai = geneFaux;
            geneFaux = tmp;
        }
        if (isNode()) {
            geneVrai.mutation(probaMutationAction, probaMutationCondition, probaMutationEchange);
            geneFaux.mutation(probaMutationAction, probaMutationCondition, probaMutationEchange);
        }
    }

    /**
     * appel par default à forceMutation, le gène actuel étant égal à sa contrepartie dans son père, on connait déjà le type interdit
     */
    public void forceMutation() {
        forceMutation(this.type);
    }

    /**
     * Force le Gene actuel à changer de signature
     * Il prend également en paramètre le GeneType dont il doit être différent
     *
     * @param forbidden le GeneType interdit
     */
    public void forceMutation(GeneType forbidden) {
        // mutation

        // si le gene est un noeud, on retransmet aleatoirement l'ordre pour aller modifier un feuille
        if (isNode()) {
            boolean fOrV = RANDOM_GENERATOR.nextBoolean();
            (fOrV ? geneVrai : geneFaux).forceMutation((fOrV ? geneFaux : geneVrai).getType());
        }
        // on ne force la mutation que sur une feuille
        else {
            // on liste les types disponibles pour ne pas faire de conflit
            ArrayList<GeneType> geneDisp = new ArrayList<>();
            for (int i = GeneType.NODE_COUNT.ordinal() + 1; i < GeneType.LEAF_COUNT_AND_NODE_COUNT_AND_ONE.ordinal(); i++) {
                if (GeneType.values()[i] != forbidden && GeneType.values()[i] != this.type) {
                    geneDisp.add(GeneType.values()[i]);
                }
            }
            //on choisi un type au hazard
            type = geneDisp.get(RANDOM_GENERATOR.nextInt(geneDisp.size()));
        }

    }

    /**
     * Cloneable implique que clone est Override
     * Assure que le clone et l'original sont indépendants
     * La propriété Root est definie dans le constructeur de Gene(GeneType,Gene,Gene)
     */
    @Override
    public Gene clone() {
        if (isNode())
            return new Gene(type, geneVrai.clone(), geneFaux.clone());
        else
            return new Gene(type);
    }

    @Override
    public String toString() {
        String ret = type.name();
        if (isNode()) ret += "[" + geneVrai + ", " + geneFaux + "]";

        return ret;
    }

    public boolean equals(Gene g) {
        if (g == null) return false;
        return this.type == g.type &&
                (this.geneVrai == null || this.geneVrai.equals(g.geneVrai)) &&
                (this.geneFaux == null || this.geneFaux.equals(g.geneFaux));
    }

    /**
     * Par exemple DETECTE_NOURRITURE[DEPLACEMENT_GAUCHE, DEPLACEMENT_DROIT] retourne 3
     *
     * @return le nombre de sous-gène (en comptant la racine)
     */
    public int getNbSousGene() {
        if (isNode())
            return 1 + geneVrai.getNbSousGene() + geneFaux.getNbSousGene();

        return 1;
    }

    /**
     * retourne un sous-gène avec son id, l'id est donnée par ordre croissant dans un parcours de l'arbre en profondeur par la gauche (côté vrai) avec 0 représentant la racine
     *
     * @param i l'id
     * @return le sous-gène correspondant
     */
    public Gene getSousGeneById(int i) {
        if (i == 0) return this;
        int gv = geneVrai.getNbSousGene();
        if (i <= gv) return geneVrai.getSousGeneById(i - 1);
        return geneFaux.getSousGeneById(i - gv - 1);
    }

    /**
     * revoie le Gene suivant en fonction du choix
     */
    public Gene getNext(boolean choix) {
        if (!isNode()) return null;
        if (choix) {
            return geneVrai;
        } else {
            return geneFaux;
        }
    }

    /**
     * retourne la hauteur de l'arbre, par convention la hauteur d'une feuille est 0.
     *
     * @return la hauteur de l'arbre
     */
    public int getHeight() {
        if (isNode()) {
            return 1 + Math.max(geneVrai.getHeight(), geneFaux.getHeight());
        } else {
            return 0;
        }
    }

    public boolean isNode() {
        return type.ordinal() < GeneType.NODE_COUNT.ordinal();
    }

    public GeneType getType() {
        return type;
    }

    public boolean isRoot() {
        return b_isRoot;
    }
}
